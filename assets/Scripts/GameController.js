var AbstractScreen = require('./Utils/Screens/AbstractScreen');
var PlayerView = require('./PlayerView');
var BoardView = require('./BoardView');
var BroadcastNames = require('./Constants').BroadcastNames;
var EventNames = require('./Constants').EventNames;
var PlayerStates = require('./Constants').PlayerStates;
var ChatType = require('./Constants').ChatType;
var Dice = require('./Dice');
var ChatPanel = require('./UI/ChatPanel')
const EventEmitter = require('./Utils/EventEmitter');
const VIDEOPANEL = require('./UI/VideoPanel');
const Constants = require("./VideoChat/PeerConstants");
cc.Class({
    extends: AbstractScreen,

    properties: {
        playerViews: {
            type: PlayerView,
            default: []
        },
        boardView: {
            type: BoardView,
            default: null
        },
        chatPanel: {
            type: ChatPanel,
            default: null
        },
        chatBox: {
            type: cc.EditBox,
            default: null
        },
        rollButton: {
            type: cc.Node,
            default: null
        },
        turnLbl: {
            type: cc.Label,
            default: null
        },
        emojiPanel: {
            default: null,
            type: cc.Node
        },
        videoPanel: {
            default: null,
            type: VIDEOPANEL
        }
    },
    onLoad() {
        EventEmitter.inst.removeAllListener();
        EventEmitter.inst.on("send", function () {
            this.sendChat();
        }.bind(this));
        if (!videoEnabled)
            return;
        window.VideoChatManager.eventEmitter.on(Constants.VIDEOCALL_EVENTS.ON_CONNECT, this.onConnection.bind(this));
        window.VideoChatManager.eventEmitter.on(Constants.VIDEOCALL_EVENTS.ON_CONNECTION_REQUEST, this.onConnectionRequest.bind(this));
        window.VideoChatManager.eventEmitter.on(Constants.VIDEOCALL_EVENTS.ON_PEER_DISCONNECT, (() => {
            var obj = {
                'type': BroadcastNames.REJECTCALL
            }
            this.room.send(obj);
        }).bind(this));

        window.VideoChatManager.eventEmitter.on(Constants.VIDEOCALL_EVENTS.ON_MAKE_CALL, (() => {
            console.log("ON_MAKE_CALL FIRED");
            var obj = {
                'type': BroadcastNames.MAKECALL
            }
            this.room.send(obj);
        }).bind(this));

    },

    changeVideoStatusLocal(e, videoType) {
        this.videoPanel.changeUserVideoStatus(videoType, (data) => {
            this.room.send(data);
            this.playerViews[0].changeSpriteState(data);
        });
    },

    onConnection(cameraId, canCall, peerid) {
        console.log("onConnection canCall " + canCall + " peerId " + peerid);
        if (canCall)
            this.videoPanel.makeCall(peerid);
        this.playerViews[0].changeSpriteState({
            "audio": true,
            "video": true
        });
        this.playerViews[1].changeSpriteState({
            "audio": true,
            "video": true
        });
        this.playerViews[1].onProfileClick();
    },

    onConnectionRequest(connection) {
        console.log("onConnectionRequest ", connection.peer);
        this.playerViews[1].setCallReceiveView();
    },

    onRejectCall() {
        this.playerViews[1].setCallView();
        this.videoPanel.rejectCall();
    },

    onShow: function (data) {
        this.chatPanel.resetChatPanel();
        this.otherPlayers = 0;
        this.players = {};
        this.room = data;
        this.myId = data.sessionId;
        cc.sys.localStorage.setItem("roomId", roomId);
        cc.sys.localStorage.setItem("gameUrl", gameUrl);
        cc.sys.localStorage.setItem("myId", this.myId);
        this.registerRoomEvents();
        this.boardView.resetPlayerIcons();
        this.setPlayers();
    },
    onChatMessage(data) {
        if (data.clientId === this.myId)
            return;
        var chatObj = {
            "message": BroadcastNames.CHAT,
            'type': ChatType.OTHER,
            "msg": data.msg
        }
        this.onChat(chatObj);
    },

    registerRoomEvents: function () {
        var self = this;
        var self = this;
        this.room.state.players.onRemove = function (player, sessionId) {
            self.onPlayerLeave(sessionId);
        };
        this.room.state.players.onChange = (player, key) => {
            self.onPlayerChanged(key);
        };
        this.room.state.onChange = (changes) => {
            changes.forEach(change => {
                if (change.field == "roomState") {
                    self.onRoomStateChange();
                }
            });
        };
        this.room.onMessage(function (broadcast) {
            console.log("broadcast is  ", broadcast);
            switch (broadcast.message) {
                case BroadcastNames.ON_TURN:
                    self.onTurn();
                    break;
                case BroadcastNames.START_GAME:
                    self.startGame(broadcast);
                    break;
                case BroadcastNames.ON_DICE_ROLL:
                    self.onDiceRoll();
                    break;
                case BroadcastNames.ON_DICE_OUTCOME:
                    self.onDiceOutCome(broadcast);
                    break;
                case BroadcastNames.ON_RECONNECTED:
                    self.onPlayerReconnected(broadcast);
                    break;
                case BroadcastNames.GAME_OVER:
                    self.onGameOver(broadcast);
                    break;
                case BroadcastNames.ON_CHAT:
                    broadcast["type"] = ChatType.OTHER;
                    self.onChat(broadcast);
                    break;
                case BroadcastNames.ON_VIDEOSTATUS_CHANGED:
                    self.playerViews[1].changeSpriteState(broadcast);
                    self.videoPanel.onVideoStatusChange(broadcast);
                    break;
                case BroadcastNames.ON_REJECT_CALL:
                    self.onRejectCall();
                    break;
                case BroadcastNames.ON_MAKE_CALL:
                    console.log("ON_MAKE_CALL called");
                    // self.playerViews[1].showAcceptBtn(false);
                    break;
                case BroadcastNames.REWARD:
                    window.reward = broadcast.reward;
                    console.log('reward', window.reward);
                    break;
            }
        });
        this.room.onLeave(((code) => {
            if (code == 1006) {
                this.videoPanel.stopAllVideo();
                PopUpManager.show(PopUpType.Disconnect, "Please Check Your Internet Connection");
            }
        }).bind(this));
        this.room.onError((message) => { });

    },

    onHide() {
        this.videoPanel.stopAllVideo();
    },

    onRoomStateChange() { },

    onPlayerChanged(playerId) {
        switch (this.room.state.players[playerId].state) {
            case PlayerStates.DISCONNECTED:
                this.onPlayerDisconnected(playerId);
                break;
        }
    },

    setPlayers: function () {

        let playerIds = Object.keys(this.room.state.players);
        for (let i = 0; i < playerIds.length; i++) {
            this.setPlayerView(this.room.state.players[playerIds[i]], this.room.state.players[playerIds[i]].id);
        }
        if (this.room.state.currentTurn != "") {
            this.onTurn();
        }
    },

    setPlayerView: function (player, sessionId) {
        this.sessionid = sessionId;
        if (sessionId == this.myId) {
            this.players[sessionId] = this.playerViews[0];
            this.boardView.setIcon(0, player.avatarId);
        } else {
            this.otherPlayers++;
            var playerPos = this.clamp(this.otherPlayers, 1, this.playerViews.length - 1);
            this.players[sessionId] = this.playerViews[playerPos];
            this.boardView.setIcon(1, player.avatarId);
        }
        this.players[sessionId].setPlayer(player, sessionId == this.myId);
    },

    /**
     * 
     */
    startGame: function () { },

    MovePlayer(data) {
        this.playersViews[this.room.state.currentTurn].MovePlayer(data.previousPosition, data.nextPosition, data.diceOutcome);
    },

    onTurn: function () {
        var playerIds = Object.keys(this.room.state.players);
        for (let i = 0; i < playerIds.length; i++) {
            if (this.room.state.currentTurn == playerIds[i]) {
                this.players[playerIds[i]].activateTurn();
            } else {
                this.players[playerIds[i]].deactivateTurn();
            }
        }
        if (this.room.state.currentTurn == this.myId) {
            this.turnLbl.string = "Your Turn"
            SoundManager.playSound(GameSFX.Timer_Loop, true);
            this.rollButton.active = true;
        } else {
            this.turnLbl.string = "Opponent's Turn";
            this.rollButton.active = false;
        }
    },

    onDiceRoll() {
        this.players[this.room.state.currentTurn].rollDice();
    },

    onDiceOutCome(data) {
        this.players[this.room.state.currentTurn].onDiceOutCome(data.diceOutcome);
        setTimeout((() => {
            this.currentPlayer = this.room.state.players[this.room.state.currentTurn];
            if (this.room.state.currentTurn == this.myId)
                this.icon = 0;
            else this.icon = 1;
            this.boardView.playerPosition(data, this.icon);
        }).bind(this), 1000);


    },


    onPlayerLeave: function (playerId) {
        this.videoPanel.disconnectPlayer(this.room.state.players[playerId].peerid);
    },
    onPlayerDisconnected: function (playerId) {
        this.players[playerId].onDisconnected();
        this.videoPanel.disconnectPlayer(this.room.state.players[playerId].peerid);
    },

    onPlayerReconnected: function (broadcast) {
        if (broadcast.pId == this.myId) {
            var elapsedTime = (broadcast.currentTime - this.room.state.timeStamp) / 1000;
            this.setPlayers();
            this.players[this.room.state.currentTurn].activateTurn(elapsedTime);
        } else {
            this.players[broadcast.pId].onReconnected();
        }
    },

    onGameOver: function (broadcast) {
        var data = broadcast;
        console.log("DTA ON GAMEOVER --", data);
        if (broadcast.pId == this.myId) {
            SoundManager.playSound(GameSFX.Game_Win, false);
            PopUpManager.show(window.prizeMode == "COUPON" ? PopUpType.WinCoupon : PopUpType.Win, data);
        } else {
            data.winnerText = "Player " + broadcast.pId + "  Won";
            SoundManager.playSound(GameSFX.Game_Lost, false);
            PopUpManager.show(window.prizeMode == "COUPON" ? PopUpType.LoseCoupon : PopUpType.Lose);
        }
        cc.sys.localStorage.removeItem("roomId");
        cc.sys.localStorage.removeItem("sessionId");
        this.videoPanel.stopAllVideo();
    },



    clamp(num, min, max) {
        return num <= min ? min : num >= max ? max : num;
    },

    onDiceClick: function () {
        this.players[this.room.state.currentTurn].stopTimer();
        this.room.send({ "type": "Roll" });
        this.onButtonClick();
        this.rollButton.active = false;
    },

    leaveGame() {
        console.log("ONN LEAEVEEEEE")
        this.room.leave();
        console.log("ONN 11111111111")
        this.videoPanel.stopAllVideo();
        console.log("ONN 2222222222222222222")
        ScreenManager.showScreen(ScreenEnum.LoginScreen, null, function () {
            console.log("ONN 44444444444444")
        })
        console.log("ONN 333333333333333")
    },

    onBackButtonClick() {
        var self = this;
        this.onButtonClick();
        PopUpManager.show(PopUpType.Exit, {
            leave: function () {
                self.leaveGame();
            },
            resume: function () {
                if (self.room.state.currentTurn == self.myId)
                    SoundManager.playSound(GameSFX.Timer_Loop, true);
            }
        });
    },

    reConnect(data) {
        this.otherPlayers = 0;
        this.players = {};
        this.room = data;
        SESSION_DATA.ROOM_ID = data.id;
        this.myId = data.sessionId;
        cc.sys.localStorage.setItem("myId", this.myId)
        this.registerRoomEvents();
        this.setPlayers();

    },

    onChat(chatBroadcast) {

        this.chatPanel.onChat(chatBroadcast);
    },

    sendChat() {
        if (this.chatBox.string == "") {
            return;
        }
        if (this.emojiPanel.active)
            this.emojiPanel.active = false;
        var msg = this.chatBox.string;
        this.chatBox.string = "";
        var chatObj = {
            "message": BroadcastNames.CHAT,
            'type': BroadcastNames.CHAT,
            "msg": msg
        }
        this.room.send(chatObj);
        var chatObj = {
            "message": BroadcastNames.CHAT,
            'type': ChatType.SELF,
            "msg": msg
        }
        this.onChat(chatObj);
    },



});