var AbstractScreen = require('./Utils/Screens/AbstractScreen');
var MatchMakingPlayerView = require('./MatchMakingPlayerView');
var BroadcastNames = require('./Constants').BroadcastNames;
var EventNames = require('./Constants').EventNames;
var RoomStates = require('./Constants').RoomStates;


cc.Class({
    extends: AbstractScreen,

    properties: {

        playerViews: {
            default: [],
            type: MatchMakingPlayerView
        },

        animePlay: {
            type: cc.Node,
            default: null
        },

        gameFeeLbl: {
            type: cc.Label,
            default: null
        }

    },
    onLoad() { },



    onShow(data) {
        //animePlay.play();
        this.animePlay.active = true;

        this.playersCount = 0;
        this.room = data;
        this.myId = data.sessionId;
        for (let i = 0; i < this.playerViews.length; i++) {
            this.playerViews[i].reset();
        }
        this.registerRoomEvents();
        this.otherCount = 0;
        this.gameFeeLbl.string = ""; //BIND_DATA.GAMEFEE + "Coins";
    },

    registerRoomEvents: function () {
        var self = this;
        this.room.state.players.onAdd = (player, sessionId) => {
            // if (sessionId != this.myId) {
            //     this.animePlay.active = false;
            //     this.avatar1.active = (player.avatarId === 0);
            //     this.avatar2.active = (player.avatarId === 1);
            // }
            self.onPlayerAdded(player, sessionId);
        };

        this.room.state.players.onRemove = function (player, sessionId) {
            self.onPlayerLeave(sessionId);
        };
        this.room.onMessage((broadcast) => {
            switch (broadcast.message) {
                case BroadcastNames.START_GAME:
                    self.startGame();
                    break;
                case BroadcastNames.ON_PLAYER_LEAVE:
                    self.onPlayerLeave(broadcast.pId);
                    break;
                case BroadcastNames.GAME_NOT_FOUND:
                    self.onGameNotFound();
                    break;
                case BroadcastNames.ON_PEER_CONNECTED:
                    window.opponentPeerId = broadcast.peerId;
                    console.log("oppnent Peer Id on BroadCast ", window.opponentPeerId);
                    break;
            }
        });
    },


    onRoomStateChange() {
        switch (this.room.state.roomState) {
            case RoomStates.RUNNING:
                this.startGame();
                break;
        }
    },

    onPlayerAdded(player, sessionId) {
        this.playersCount++;
        if (sessionId == this.myId) {
            this.playerViews[0].setPlayer(player);
            console.log('player', player);
            if (videoEnabled) {
                setTimeout(function () {
                    this.connectVideoCall();
                }.bind(this), 250);
            }
        } else {
            SoundManager.playSound(GameSFX.Game_Matched, false);
            this.otherCount++;
            this.playerViews[this.otherCount].setPlayer(player);

        }
    },

    connectVideoCall() {
        let roomPlayers = Object.keys(this.room.state.players);
        let peerIds = [];
        for (let i = 0; i < roomPlayers.length; i++) {
            console.log("Peer Ids For Video Call " + this.room.state.players[roomPlayers[i]].peerid);
            if (this.room.state.players[roomPlayers[i]].peerid !== window.peer.id && this.room.state.players[roomPlayers[i]].peerid !== "")
                peerIds.push(this.room.state.players[roomPlayers[i]].peerid);
        }
        window.opponentPeerId = peerIds[0] === undefined ? "" : peerIds[0];
        console.log("oppnent Peer Id for connectVideoCall ", window.opponentPeerId);
        //  window.VideoChatManager.connectToPeers(peerIds);
    },

    onPlayerLeave(sessionId) {
        for (let i = 0; i < this.playerViews.length; i++) {
            if (this.playerViews[i].playerId == sessionId) {
                this.playerViews[i].reset();
                this.playersCount--;
            }
        }
    },

    startGame: function () {

        ScreenManager.showScreen(ScreenEnum.Gameplay, this.room, function () { });
    },

    onBackButtonClick() {
        this.onButtonClick();
        this.room.leave();
        ScreenManager.showScreen(ScreenEnum.LoginScreen, null, function () { });
    },

    onGameNotFound() {
        PopUpManager.show(PopUpType.No_Opponent, null);
        ScreenManager.showScreen(ScreenEnum.LoginScreen, null, function () { });
    },

    onSoundBtn() {
        this.soundActive.active = true;
        SoundManager.muteSound(false);
    },
    offSoundBtn() {
        this.soundActive.active = false;
        SoundManager.muteSound(true);
    }
});