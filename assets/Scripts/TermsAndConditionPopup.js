var PopUpBase = require('PopUpBase');
cc.Class({
    extends: PopUpBase,

    properties: {
        content: {
            type: cc.Label,
            default: null,
        }
    },

    onShow(data) {
        if(data)
        this.content.string = data;

    },

    closePopUp() {
        this.onButtonClick();
        PopUpManager.hide(PopUpType.TermsAndCondition);
    }
});
