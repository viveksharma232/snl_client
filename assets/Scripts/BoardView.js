cc.Class({
    extends: cc.Component,

    properties: {
        board: {
            default: null,
            type: cc.Node,
        },
        playerIcon: {
            type: cc.Node,
            default: []
        },
        tiles: {
            type: cc.v2,
            default: []
        },
        boardCellWidth: {
            default: 0,
            type: cc.Integer
        },
        boardCellHeight: {
            default: 0,
            type: cc.Integer
        },
        startPosition: {
            default: null,
            type: cc.Node,
        },
        startTile: {
            default: null,
            type: cc.Node,
        },
        snakesAnimation: {
            default: [],
            type: [cc.Animation]
        }


    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {

        this.boardCellWidth = 104;
        this.boardCellHeight = 104;
        this.startPos = this.startTile.position;
        this.setTiles();
    },

    testing() {
        this.snakesAnimation[0].play();
    },

    resetPlayerIcons() {
        this.playerIcon[0].position = this.startPosition.position;
        this.playerIcon[1].position = this.startPosition.position;
    },

    setTiles() {
        for (let i = 0; i <= 99; i++) {
            var x = parseInt(i % 10);
            var y = parseInt(i / 10);
            if (y % 2 == 0) {
                this.tiles.push(new cc.v2(this.startPos.x + x * this.boardCellWidth, this.startPos.y + y * this.boardCellHeight));
            } else {
                this.tiles.push(new cc.v2(this.startPos.x + (9 - x) * this.boardCellWidth, this.startPos.y + y * this.boardCellHeight));
            }
        }
    },


    setIcon(icon, avatarId) {
        this.playerIcon[icon].children[0].children[0].getComponent(cc.Sprite).spriteFrame = Avatars.avatarSprites[avatarId];
    },

    playerPosition(data, currentPlayer) {
        console.log("playerPositionplayerPositionplayerPositionplayerPosition ");
        let prepos = data.previousPosition;
        let nexpos = data.nextPosition;
        let diceOut = data.diceOutcome;
        let moveType = data.moveType;
        this.checkSnakeOrLadder(prepos, nexpos, diceOut, currentPlayer, moveType);
    },


    checkSnakeOrLadder(prePos, nextPos, diceOut, currentPlayer, moveType) {

        if (prePos == nextPos) {
            return;
        }
        console.log("fdsfjsdfkldsjfkl ");
        var self = this;
        this.moveHops(prePos, prePos + diceOut, currentPlayer, () => {
            if (nextPos != prePos + diceOut) {
                if (moveType === "Snake")
                    self.moveThroughSnake(nextPos, currentPlayer);
                else
                    self.moveDirect(nextPos, currentPlayer);
            }
        });
    },

    moveHops(prePos, nextPos, currentPlayer, cb) {
        var self = this;
        var i = prePos;
        cc.director.getScheduler().schedule(function() {
            if (i == nextPos) {
                cc.director.getScheduler().unscheduleAllForTarget(self);
                cb();
                return;
            }
            i++;
            this.moveToNextTile(self.tiles[i - 1], self.playerIcon[currentPlayer]);
        }, this, 0.3);
    },
    moveDirect(nextPos, currentPlayer) {
        SoundManager.playSound(GameSFX.Game_Ladder, false);
        console.log("Ladders next Ps " + nextPos);
        this.finalPos = this.tiles[nextPos - 1];
        var Move = cc.moveTo(1, this.finalPos);
        Move.easing(cc.easeInOut(3.0));
        this.playerIcon[currentPlayer].runAction(Move);
    },

    moveThroughSnake(nextPos, currentPlayer) {
        SoundManager.playSound(GameSFX.Game_Snake, false);
        this.finalPos = this.tiles[nextPos - 1];
        this.current = currentPlayer;
        let animationIndex = this.getAnimationIndex(nextPos);
        this.playerIcon[this.current].parent = this.snakesAnimation[animationIndex].node;
        this.playerIcon[this.current].position = cc.v2(0, 0);
        this.playerIcon[this.current].scale = 1 / this.snakesAnimation[animationIndex].node.parent.scale;
        this.snakesAnimation[animationIndex].play();
    },

    getAnimationIndex(nextPos) {
        for (let i = 0; i < this.snakesAnimation.length; i++) {
            if (this.snakesAnimation[i].node.name === nextPos.toString())
                return i;
        }
    },

    onAnimationComplete(naam) {
        console.log("onAnimationComplete called " + naam);
        this.playerIcon[this.current].parent = this.node;
        this.playerIcon[this.current].position = this.finalPos;
        this.playerIcon[this.current].scale = 1;
    },

    moveToNextTile(nextTilePos, playerIcon) {
        var Move = cc.moveTo(0.2, nextTilePos);
        Move.easing(cc.easeInOut(3.0));
        playerIcon.runAction(Move);
    }
});