cc.Class({
    extends: cc.Component,

    properties: {
        avatarSprites: {
            default: [],
            type: [cc.SpriteFrame]
        },
        avatarBgColor: {
            default: [],
            type: [cc.Color]
        },
        myUserColor: cc.Color,
        opponentColor: cc.Color,
        myUserTextColor: cc.Color,
        opponentTextColor: cc.Color,
    },


    start() {
        window.Avatars = this;
    },

    getAvatarIndexFromName(avatarName) {
        for (var i = 0; i < this.avatarSprites.length; i++) {
            if (this.avatarSprites[i].name.toLowerCase() === avatarName.toLowerCase()) {
                return i;
            }
        }
        return 12;
    }

});
