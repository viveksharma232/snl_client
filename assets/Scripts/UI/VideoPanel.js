const Constants = require('../VideoChat/PeerConstants');

cc.Class({
    extends: cc.Component,

    properties: {


    },
    onLoad() {
        window.VideoChatManager.eventEmitter.on(Constants.VIDEOCALL_EVENTS.ON_CONNECT, this.onConnection.bind(this));
        window.VideoChatManager.eventEmitter.on(Constants.VIDEOCALL_EVENTS.INTERNAL_VIDEO_STATUS, this.onChangeVideoStatus.bind(this));
        window.VideoChatManager.eventEmitter.on(Constants.VIDEOCALL_EVENTS.ON_PERMISSION_GIVEN, this.onPermissionGiven.bind(this));
        this.othAudio = this.othVideo = this.myAudio = this.myVideo = true;
    },

    onConnection(cameraId) {
        //  window.VideoChatManager.muteByCameraId(cameraId);
    },

    rejectCall() {
        window.VideoChatManager.disconnectPeerView(1);
    },


    changeUserVideoStatus(contentType, callback) {
        if (window.peer.id === "" || Object.keys(window.VideoChatManager.peerNetworks).length === 0)
            return;

        if (contentType == 'audio')
            this.myAudio = !this.myAudio;

        if (contentType == 'video')
            this.myVideo = !this.myVideo;

        let statusData = {
            "type": "VideoStatus",
            "audio": this.myAudio,
            "video": this.myVideo
        };
        callback(statusData);

    },

    onChangeVideoStatus(contentType) { //isAudio, isVideo, cameraId -- parameter used before.
        if (contentType == 'audio') {
            this.othAudio = !this.othAudio;
        }
        if (contentType == 'video') {
            this.othVideo = !this.othVideo;
        }

    },

    onVideoStatusChange(data) {
        if (data.audio) {
            window.VideoChatManager.peerNetworks[data.peerId].resumePeerAudio();
        } else {
            window.VideoChatManager.peerNetworks[data.peerId].stopPeerAudio();
        }

        if (data.video) {
            window.VideoChatManager.peerNetworks[data.peerId].resumePeerVideo();
        } else {
            window.VideoChatManager.peerNetworks[data.peerId].stopPeerVideo();
        }
    },

    //Video Chat Addition

    onPermissionGiven(Permission) {
        if (Permission) {
            // window.VideoChatManager.initializePeer(window.stunServer, window.turnServer);
            if (PopUpManager.currentOverLayedPopUps.indexOf(PopUpType.Permission) !== -1)
                PopUpManager.hide(PopUpType.Permission);
            return;
        }
        if (PopUpManager.currentOverLayedPopUps.indexOf(PopUpType.Permission) == -1)
            PopUpManager.show(PopUpType.Permission, {});
    },

    disconnectPlayer(peerId) {
        window.VideoChatManager.disconnectWithPeerId(peerId);
    },

    stopAllVideo() {
        window.VideoChatManager.destroyAllNetworks();
    }

    ,


    makeCall(peerId) {
        window.VideoChatManager.makePeerCall(peerId);
    }

    //        window.VideoChatManager.unMuteAllConnections();


});