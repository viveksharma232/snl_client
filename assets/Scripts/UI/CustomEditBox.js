
cc.Class({
    extends: cc.Component,

    properties: {
        editBoxLbl: {
            type:cc.Label,
            default : null
        },
        richTextLbl :{
            type:cc.RichText,
            default : null
        }
    },

    onTextChange(){
        this.richTextLbl.string = this.editBoxLbl.string;
    }

});
