var ChatItem = require('./ChatItem');
const EventEmitter = require('../Utils/EventEmitter');
cc.Class({
    extends: cc.Component,

    properties: {

        chatItems: {
            type: cc.Prefab,
            default: []
        },
        contentNode: {
            default: null,
            type: cc.Node
        },
        emojiPanel: {
            default: null,
            type: cc.Node
        },
        chatBox: {
            type: cc.EditBox,
            default: null
        },
        richTextLbl: {
            type: cc.RichText,
            default: null
        },
        labelText: {
            type: cc.Label,
            default: null
        },
        placeholder: {
            type: cc.Node,
            default: null
        }

    },

    start() {},

    onChat(chat) {
        console.log(chat)
        var newChat = cc.instantiate(this.chatItems[chat.type]);
        let msgHeight = newChat.getComponent('ChatItem').setMessage(chat.msg);
        console.log("msg hieght is ", msgHeight);
        if ((this.currentSize + msgHeight) >= this.contentNode.height)
            this.contentNode.height += msgHeight;
        this.currentSize += msgHeight;
        this.contentNode.addChild(newChat);
        newChat.setSiblingIndex(0);
    },

    on_offEmojiPanel() {
        if (this.emojiPanel.active)
            this.disableEmojiPanel();
        else
            this.enableEmojiPanel();

    },

    enableEmojiPanel() {
        this.emojiPanel.active = true;
        //this.richTextLbl.string = "";
        //this.richTextLbl.node.active = true;
        this.labelText.node.active = false;
        this.placeholder.active = false;
    },

    disableEmojiPanel() {
        this.emojiPanel.active = false;
        this.richTextLbl.string = "";
        //this.richTextLbl.node.active = false;
        this.labelText.node.active = true;
        this.placeholder.active = true;
    },

    onEmojiClick(event, customEventData) {
        // if (this.chatBox.string.length > 0) {
        //     EventEmitter.inst.emit("send");
        //     this.chatBox.string = "";
        // }

        this.chatBox.string += '<img src=\'' + customEventData + '\' click=\'None\' />';
        this.labelText.node.active = false;
        this.placeholder.active = false;
        this.richTextLbl.string = this.chatBox.string;
        //this.chatBox.string = '<img src=\'2\' click=\'handle\' />';
        //console.log('richTxtLbl',this.richTxtLbl.string);
        EventEmitter.inst.emit("send");
    },
    resetChatPanel() {
        this.contentNode.removeAllChildren();
        this.chatBox.string = "";
        this.richTextLbl.string = "";
        this.contentNode.height = 350;
        this.currentSize = 0;
    }
});