cc.Class({
    extends: cc.Component,

    properties: {
        msgLbl: {
            type: cc.RichText,
            default: null
        }
    },

    setMessage(msg) {
        this.wordsPerRow = 40;
        this.singleLineHeight = this.msgLbl.lineHeight;
        console.log("msg to set ", msg);
        let line = this.breakMessageInLine(msg);

        console.log("after modification ", line);
        this.msgLbl.string = line.newmsg;
        return this.node.height = line.linecount * this.singleLineHeight;
    },


    breakMessageInLine(msg) {
        let noOfRows = Math.ceil(msg.length / this.wordsPerRow);

        for (let i = 1; i < noOfRows.length; i++) {
            this.insertString(msg, (this.wordsPerRow * 1 + (i - 1)), '\n');
        }

        return {
            "newmsg": msg,
            "linecount": noOfRows
        };
    },

    insertString(str, index, value) {
        return str.substr(0, index) + value + str.substr(index);
    }

});