cc.Class({
    extends: cc.Component,

    properties: {

        nameLbl: {
            type: cc.Label,
            default: null
        },
        reelAnim: {
            default: null,
            type: cc.Animation
        },
        defaultAvtar: {
            type: cc.Sprite,
            default: null
        },

    },


    onEnable() {
        if (this.reelAnim) {
            this.reelAnim.node.active = true;
            this.reelAnim.play();
            this.defaultAvtar.node.active = false;
        }
    },

    setPlayer(player) {
        this.playerId = player.id;
        this.nameLbl.string = player.name;
        console.log('Player' + player.avatarId);
        this.defaultAvtar.spriteFrame = Avatars.avatarSprites[player.avatarId];
        if (this.reelAnim) {
            this.defaultAvtar.node.active = true;
            this.reelAnim.stop();
            this.reelAnim.node.active = false;
        }
    },

    reset() {
        this.nameLbl.string = "";
        this.playerId = "";
    }


})