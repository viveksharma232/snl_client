import API from './Utils/API';
import { APINAME } from "./Constants";
//import Factory from '../Analytics/Factory';
var AbstractScreen = require("./Utils/Screens/AbstractScreen");
var Colyseus = require('./Colyseus/colyseus');
var EventEmitter = require('./Utils/EventEmitter');
var Constants = require('./Constants');
const { GAME_CONST } = require('./Constants');
const Fingerprint2 = require("fingerprintjs2")

cc.Class({
    extends: AbstractScreen,

    properties: {
        sfxDisabledIcon: {
            type: cc.Node,
            default: null,
        },
        musicDisabledIcon: {
            type: cc.Node,
            default: null,
        },
        winPoints: {
            type: cc.Label,
            default: null
        },
        onlinePlayersNo: {
            type: cc.Label,
            default: null
        },
        playBtn: {
            type: cc.Node,
            default: null
        },
        logoAnim: {
            default: null,
            type: cc.Animation
        },
        buttonAnim: {
            default: null,
            type: cc.Animation
        },
        userAvatar: {
            type: cc.Sprite,
            default: null
        },
        userAvatarBG: {
            type: cc.Node,
            default: null
        },
        userNameLabel: {
            type: cc.Label,
            default: null
        },
        totalWinLabl: {
            type: cc.Label,
            default: null
        },
        balanceContainers: {
            type: cc.Node,
            default: []
        },
        balanceLbls: {
            type: cc.Label,
            default: []
        },
        balanceIcons: {
            type: cc.Sprite,
            default: []
        },
        defaultCurrencyIcon: {
            type: cc.SpriteFrame,
            default: null
        },
        gameFeeLabel: {
            type: cc.Label,
            default: null
        },
        playBtnLbl: {
            type: cc.Label,
            default: null
        }
    },

    onLoad() {
        window.api = new API(); // 
        //let factory = new Factory();
        window.defaultCurrencyIcon = this.defaultCurrencyIcon;
        this.disableBalanceContainers();
        var url_string = window.location.href;
        var url = new URL(url_string);
        window.gameId = url.searchParams.get("gid") || 4;
        this.key = url.searchParams.get("key")
        //window.Analytics = factory.getInstance();
        //Analytics.customEvent(Analytics_Event.GAME_LAUNCH, Analytics_Screen.PORTAL);
        window.URLS = {};
        URLS.CURRENT_GAME = url_string;
        URLS.MORE_GAMES = "https://huntergames.in";// "https://" + url.host;
        URLS.SHARE_LINK = "https://" + url.host + url.pathname;
        window.MERCHANT_HOST = "mhg.huntergames.live";
        if (url.searchParams.get("m")) {
            window.MERCHANT_PATH = "/" + url.searchParams.get("m");
        } else if (url.searchParams.get("mid")) {
            window.MERCHANT_PATH = "/" + url.searchParams.get("mid");
        } else {
            window.MERCHANT_PATH = "/free-merchant";
        }
    },

    start() {
        //ScreenManager.sounds.playScreenMusic(ScreenEnum.LoginScreen);
        this.registerAdEvents();
        if (this.key) {
            console.log('Trying to log in With Key...');
            this.getConfigWithKey(this.key);
            return;
        }

        this.lobby = null;
        PopUpManager.show(PopUpType.Loading, {}, () => { });

        this.loginRequest("");
    },

    // onEnable() {
    //     this.logoAnim.play();
    //     setTimeout(() => {
    //         this.buttonAnim.play();
    //     }, 200);
    // },

    setMusicStatus() {
        return;
        let sfxValue = api.getLocalUserData(GAME_CONST.SFX_STATUS);
        let musicValue = api.getLocalUserData(GAME_CONST.MUSIC_STATUS);
        let sfx = sfxValue !== undefined ? sfxValue : 1;
        let music = musicValue !== undefined ? musicValue : 1;
        SoundManager.setSFXVolume(parseInt(sfx));
        SoundManager.setMusicVolume(parseInt(music));
        this.sfxDisabledIcon.active = parseInt(sfx) ? false : true;
        this.musicDisabledIcon.active = parseInt(music) ? false : true;
    },

    onShow(data) {
        this.disableBalanceContainers();
        this.setUIData();
        this.checkBalance();
        this.registerAdEvents();
        this.setMusicStatus();
        //AdsManager.showBannerAdBottom();
        //ScreenManager.sounds.playScreenMusic(ScreenEnum.LoginScreen);
    },

    onSoundClick() {
        this.onButtonClick();
        let sfx = api.getLocalUserData(GAME_CONST.SFX_STATUS);
        //SoundManager.setSFXVolume(parseInt(sfx) ? 0 : 1, Analytics_Screen.HOME);
        this.sfxDisabledIcon.active = parseInt(sfx) ? true : false;
    },

    onMusicClick() {
        this.onButtonClick();
        let music = api.getLocalUserData(GAME_CONST.MUSIC_STATUS);
        //SoundManager.setMusicVolume(parseInt(music) ? 0 : 1, Analytics_Screen.HOME);
        this.musicDisabledIcon.active = parseInt(music) ? true : false;
    },

    disableBalanceContainers() {
        for (let i = 0; i < this.balanceContainers.length; i++)
            this.balanceContainers[i].active = false;
    },

    showHelp() {
        return;
        let isHelpRequired = api.getLocalUserData(GAME_CONST.HELP_REQUIRED);
        if (isHelpRequired) {
            api.setLocalUserData({
                [GAME_CONST.HELP_REQUIRED]: false
            })
            PopUpManager.show(PopUpType.Help, { "payoutMode": window.paymentMode });
        }
    },

    registerAdEvents() {
        return;
        EventEmitter.inst.removeAllListeners();
        EventEmitter.inst.on(Constants.Ad_Events.AD_ERROR, this.onAdError.bind(this));
        EventEmitter.inst.on(Constants.Ad_Events.ALL_ADS_COMPLETED, this.onAdComplete.bind(this));
    },

    setLoginData() {

        return new Promise((resolve, reject) => {
            var userData = api.getLocalUserData("user");
            var url = new URL(window.location.href);
            var isTokenAvailable = url.searchParams.get("user_token");
            window.defaultUserName = url.searchParams.get("defaultUserName");
            if (userData && (isTokenAvailable == "null" || !isTokenAvailable)) {
                let userDetails = userData;
                return resolve(userDetails);

            } else {
                return this.getSignatureToken()
                    .then((signature) => {
                        let
                            userDetails = {
                                usertype: "le",
                                // username: "USER" + Math.random().toString(36).slice(6),
                                avatarid: 0
                            }
                        api.setLocalUserData({
                            user: JSON.stringify(userDetails),
                            signature: signature
                        })
                        return resolve(userDetails);
                    })
                    .catch((e) => {
                        return reject(e);
                    })

            }
        })

    },

    getSignatureToken() {
        console.log("signature 1 ");
        var url = new URL(window.location.href);
        return new Promise((resolve, reject) => {
            console.log("signature 2 ");

            if (url.searchParams.get("user_token") && url.searchParams.get("user_token") != "null") {
                console.log("signature 3 ", url.searchParams.get("user_token"));
                return resolve(url.searchParams.get("user_token")); //For Jaina Or MHG
            }
            return Fingerprint2.getPromise()
                .then((data) => {
                    var values = data.map(function (component) { return component.value })
                    var murmur = Fingerprint2.x64hash128(values.join(''), 31)
                    console.log("signature 4 ", murmur);

                    return resolve(murmur)
                })
                .catch((e) => {
                    return reject(e);
                })
        })
    },

    loginRequest(key, isConfigRequired = true) {
        this.setLoginData()
            .then((data) => {
                window.userType = data.usertype;
                window.userName = data.username;
                window.loginToken = api.getLocalUserData("signature");

                let options = {
                    "token": window.loginToken,
                    "gid": window.gameId,
                    "configkey": key,
                    "username": window.userName,
                    "usertype": window.userType
                };


                PopUpManager.show(PopUpType.Loading, {}, () => { });

                window.api.getRequest(APINAME.GAMELOGIN, options, ((result) => {

                    PopUpManager.hide(PopUpType.Loading, () => { });
                    if (result["status"]["resp_code"] == 200 || result["status"]["resp_code"] == 600) {
                        window.bearertoken = result["token"];
                        // this.saveReloadUrl();
                        this.handleLoginResponse(result, !isConfigRequired);
                        if (isConfigRequired)
                            this.getConfigData();
                        else
                            this.checkBalance();
                    } else {
                        if (result["status"]["resp_code"] == 405) {
                            this.handleError(result);
                            return;
                        }
                        let data = {
                            "status": {
                                "resp_message": result["status"]["resp_message"],
                                "heading": "Whoops!",
                            }
                        }
                        this.handleError(data);
                    }
                }).bind(this));
            })
            .catch((e) => {
                console.log("Error in Set Login " + e);
            })
    },

    handleLoginResponse(result, userAvailable) {
        if (userAvailable) {
            this.setUserData(result["user"]);
            if (result["status"]["resp_code"] == 600) {
                PopUpManager.show(PopUpType.DuplicateLogin, (result["status"]["resp_message"]));
            } else {
                this.connectToGameServer();
            }
        }
        // this.setPlaceholders();
        // this.showHelp();
    },

    saveReloadUrl() {
        var url_string = window.location.href;
        api.setLocalUserData({ "reloadUrl": url_string });
        var myUrl = api.getLocalUserData("reloadUrl");
    },

    getConfigWithKey(key, isLoggedIn = false, isVerficationReq = false) {
        PopUpManager.show(PopUpType.Loading, {}, () => { });
        window.api.getRequest(APINAME.CONFIG_WITH_KEY, { "key": key }, ((result) => {

            PopUpManager.hide(PopUpType.Loading, () => { });
            if (result["status"]["resp_code"] == 200) {
                console.log("CONFIG -- ", result)
                this.handleConfigResponse(result, false);
                if (!isLoggedIn)
                    this.loginRequest(key, false);
                // if (isVerficationReq)
                //     this.verifyDebit(key);

            } else {
                if (result["status"]["resp_code"] == 405) {
                    this.handleError(result);
                    return;
                }
                let data = {
                    "status": {
                        "resp_message": result["status"]["resp_message"],
                        "heading": "Whoops!",
                    }
                }
                this.handleError(data);
            }

        }).bind(this));

    },

    getConfigData() {
        PopUpManager.show(PopUpType.Loading, {}, () => { });

        window.api.getRequest(APINAME.CONFIG, {}, ((result) => {

            PopUpManager.hide(PopUpType.Loading, () => { });
            if (result["status"]["resp_code"] == 200 || result["status"]["resp_code"] == 600) {
                this.handleConfigResponse(result, true);
            } else {
                if (result["status"]["resp_code"] == 405) {
                    return;
                    this.handleError(result);
                }
                let data = {
                    "status": {
                        "resp_message": result["status"]["resp_message"],
                        "heading": "Whoops!",
                    }
                }
                this.handleError(data);
            }

        }).bind(this));

    },

    handleConfigResponse(result, userAvailable) {
        window.gameFee = result["config"]["gamefee"];
        window.maxPlayers = result["config"]["maxplayers"];
        window.gameUrl = result["url"] ? result["url"] : result["gameurl"];

        window.videoEnabled = result["config"]["videoenable"];

        window.paymentMode = result["config"]["paymentmode"];
        window.prizeMode = result["config"]["prizemode"];
        window.merchantId = result["mid"];
        window.gameId = result["gid"];
        window.bearertoken = result["token"];
        window.lobbyId = result["lobbyId"];
        window.lobbySessionId = result["lobbySessionId"];
        window.roomId = result["roomId"];
        window.adsEnabled = result["config"]["adsenabled"] == true ? true : false;
        window.localAvatar = result.config.texts.localavatar !== undefined ? result.config.texts.localavatar.toString().toUpperCase() === "TRUE" : true;

        this.setBalanceDependencies(result);
        window.WALLETS = result.wallets;
        api.loadImage(WALLETS[prizeMode].currencyicon)
            .then((img) => {
                window.prizeIcon = img;
            })
            .catch(() => {
                window.prizeIcon = defaultCurrencyIcon;
            })

        api.loadImage(WALLETS[paymentMode].currencyicon)
            .then((img) => {
                window.paymentIcon = img;
            })
            .catch(() => {
                window.paymentIcon = defaultCurrencyIcon;
            })
        if (userAvailable) {
            this.setUserData(result["user"]);
            URLS.CLIENT_MAIN_SITE = result["config"]["texts"]["clientsite"];
            //URLS.MORE_GAMES = result["config"]["texts"]["redirect"] ? result["config"]["texts"]["redirect"] : URLS.MORE_GAMES;
            this.checkBalance();
            if (result["status"]["resp_code"] == 600) {
                PopUpManager.show(PopUpType.DuplicateLogin, (result["status"]["resp_message"]));
            } else {
                this.connectToGameServer();
            }
        } else {
            window.MERCHANT_HOST = result.host;
            window.MERCHANT_PATH = result.path;
            URLS.CLIENT_MAIN_SITE = result.merchant.clientsite;
            // URLS.MORE_GAMES = result.merchant.redirect;
        }
        this.setMusicStatus();
        this.checkURLValidtion();
    },

    checkURLValidtion() {
        let allUrls = Object.keys(URLS);
        for (let i = 0; i < allUrls.length; i++) {
            if (!URLS[allUrls[i]].includes("https://")) {
                URLS[allUrls[i]] = "https://" + URLS[allUrls[i]];
            }
        }
    },

    setUserData(user) {
        window.phone = user.phone;
        window.userID = user.userid;
        window.totalWins = user.wincount;
        window.userImageURL = user.image ? user.image : "";
        var avatarName = user.avatarid ? user.avatarid : 0;
        if (!!window.defaultUserName)
            window.userName = window.defaultUserName;
        else
            window.userName = user.name ? user.name : "GUEST";
        let userLocal = api.getLocalUserData("user")
        if (!userLocal.hasOwnProperty("username")) {
            userLocal.username = window.userName;
            api.setLocalUserData({ "user": userLocal })
        }
        if (!!avatarName) {
            avatarName = avatarName.split(".");
        }
        else {
            avatarName = "Default";
        }
        window.avatarId = Avatars.getAvatarIndexFromName(avatarName[0]);
        this.setUIData();
    },

    setBalanceDependencies(data) {
        if (!data.hasOwnProperty("wallets")) {
            window.showBalance = false;
            return;
        }

        let wallets = data.wallets;
        let allWallets = Object.keys(wallets);
        for (let i = 0; i < allWallets.length; i++) {
            if (wallets[allWallets[i]].type === "WALLET") {
                window.showBalance = true;
                return;
            }
        }
        window.showBalance = false;
    },

    setBalanceContainer(index, walletName, balanceData) {
        if (!WALLETS.hasOwnProperty(walletName))
            return;
        let wallet = WALLETS[walletName];
        this.balanceLbls[index].string = balanceData.balance;
        this.balanceContainers[index].active = true;
        let self = this;
        api.loadImage(wallet.currencyicon)
            .then((icon) => {
                self.balanceIcons[index].spriteFrame = icon;
            })
            .catch(() => {
                self.balanceIcons[index].spriteFrame = defaultCurrencyIcon
            })
    },

    handleError(data) {
        PopUpManager.hide(PopUpType.Loading);
        //data["origin"] = Analytics_Screen.HOME;
        if (typeof data["status"]["resp_message"] == "string")
            PopUpManager.show(PopUpType.Error, data);
        else if (typeof data["status"]["resp_message"] == "undefined") {
            PopUpManager.show(PopUpType.Error, data);
        } else {
            PopUpManager.show(PopUpType.Error, data);
        }
    },

    setUIData() {
        if (!!window.phone) {
            window.phone = "xxxxx" + window.phone.slice(5);
            //window.userName = window.phone;
        }
        this.userID = window.userID;
        this.maxPlayers = window.maxPlayers;
        this.totalWinLabl.string = window.totalWins;
        this.winPoints.string = ""; //parseInt(winamount) === 0 ? "Get free coupons" : ("WIN upto Rs. " + winamount);
        window.winAmount = parseInt(window.gameFee) * parseInt(this.maxPlayers);
        //this.gameFeeLabel.string = parseInt(window.gameFee) > 0 ? "with " + api.getCurrencyWithUnit(parseInt(window.gameFee), paymentMode) : "";
        //this.playBtnLbl.string = parseInt(window.gameFee) > 0 ? "PLAY NOW" : "PLAY NOW";
        //this.playBtnLbl.node.y = parseInt(window.gameFee) > 0 ? 40 : 0;
        this.setProfile();
        // AdsManager.initializeAds();
        // AdsManager.showBannerAdBottom();
    },

    setProfile() {
        this.userNameLabel.string = window.userName ? window.userName : "You";
        var self = this;
        api.loadImage(window.userImageURL)
            .then((img) => {
                window.userImage = img;
                self.userAvatar.spriteFrame = img;
                self.userAvatarBG.color = new cc.Color(Avatars.avatarBgColor[0]);
            })
            .catch(() => {
                self.userAvatar.spriteFrame = Avatars.avatarSprites[window.avatarId];
                window.userImage = Avatars.avatarSprites[window.avatarId];
                self.userAvatarBG.color = new cc.Color(Avatars.avatarBgColor[window.avatarId]);
            })
        this.userAvatarBG.getComponent(cc.Sprite).enabled = localAvatar;
    },

    connectToGameServer() {
        if (window.lobbyId && window.lobbySessionId) {
            this.tryRejoinLobby();
        } else {
            this.joinLobby();
        }
    },

    /**
     * Try To Rejoin to Lobby
     */
    tryRejoinLobby() {
        var self = this;
        PopUpManager.show(PopUpType.Loading, {});
        this.client = new Colyseus.Client(window.gameUrl);
        this.client.reconnect(window.lobbyId, window.lobbySessionId).then(room => {
            self.registerLobbyEvents();
            if (window.roomId && window.sessionId) {
                self.tryRejoin();
            }
        }).catch(err => {
            this.joinLobby();
        });
    },
    /**
     * Try to rejoin Game Room
     */
    tryRejoin() {
        var self = this;
        PopUpManager.show(PopUpType.Loading, {});
        this.client.reconnect(window.roomId, window.sessionId).then(room => {
            setTimeout(() => {
                PopUpManager.hide(PopUpType.Loading);
                var data = {
                    "roomData": room,
                };
                ScreenManager.showScreen(ScreenEnum.Gameplay, data, function () { });
            }, 2000);

        }).catch(err => {
            PopUpManager.hide(PopUpType.Loading);

        });
    },

    joinLobby(isDuplicate = false) {
        PopUpManager.show(PopUpType.Loading, {});
        var self = this;
        this.client = new Colyseus.Client(window.gameUrl);
        var options = {
            "token": window.bearertoken,
            "gid": window.gameId,
            "mid": window.merchantId,
            "name": window.userID,
            "usertype": window.userType
        };
        // if (isDuplicate) { //need to uncomment when 600 code on server will work
        options["isDuplicateLogin"] = true;
        // }

        this.client.joinOrCreate("Lobby", options).then(room => {
            self.lobby = room;
            PopUpManager.hide(PopUpType.Loading);
            window.lobbyId = room.id;
            window.lobbySessionId = room.sessionId;
            self.registerLobbyEvents();
            if (window.tid)
                self.joinRoom();
            self.showHelp();
        }).catch(err => {
            PopUpManager.hide(PopUpType.Loading);
            var data = {
                "status": {
                    "resp_message": "Lobby Authentication Failed!",
                    "heading": "WHOOPS!"
                }
            }
            self.handleError(data);
        });
    },

    OnMultiplayerClick() {
        this.onButtonClick();
        this.multiplayerGame();

        // if (window.adsEnabled) {
        //     AdsManager.showVideoAd(function () {
        //         this.multiplayerGame(Analytics_Screen.HOME);
        //     }.bind(this));
        // }
        // else {
        //     this.multiplayerGame(Analytics_Screen.HOME);
        // }

    },

    multiplayerGame() {
        //window.Analytics.viewEvent(Analytics_Screen.MULTI_GAMEPLAY, origin);
        PopUpManager.show(PopUpType.Loading, {});
        window.api.getRequest(APINAME.PLAY, {}, ((result) => {
            PopUpManager.hide(PopUpType.Loading);
            if (result["status"]["resp_code"] == 200) {
                window.gpid = result["gpid"];


                if (result.form) {
                    document.write(result.form);
                } else {
                    this.joinRoom();
                }

            } else {
                if (result["status"]["resp_code"] == 405) {
                    this.handleError(result);
                    return;
                }
                let data = {
                    "status": {
                        "resp_message": result["status"]["resp_message"],
                        "heading": "WHOOPS!",
                    }
                }

                data['enableMoreGames'] = (result.status.resp_code == 414 || result.status.resp_code == 415);

                this.handleError(data);
            }
        }).bind(this));
    },

    playPaytm() {
        window.api.getRequest(APINAME.PLAY, { url: 'http://192.168.100.46:7456' }, ((result) => {
            PopUpManager.hide(PopUpType.Loading);

            if (result["status"]["resp_code"] == 200) {
                // const popup = window.open('', 'popup', 'width=600,height=600,scrollbars=yes', false);
                // popup.document.write(result.form);
                // PopUpManager.hide(PopUpType.Loading, null, function () { });
                // this.loadingText.active = false;
                document.write(result.form);
            } else {
                // PopUpManager.hide(PopUpType.Loading, null, function () { });
                // this.loadingText.active = false;
                var data = {};
                data["title"] = "ERROR";
                data["msg"] = result["status"]["resp_message"];
                data['enableMoreGames'] = (result.status.resp_code == 414 || result.status.resp_code == 415);
                this.handleError(data);
            }
        }).bind(this));
    },


    verifyDebit() {
        // this.loadingText.active = true;
        // PopUpManager.show(PopUpType.Loading, null, function () { });
        // this.loadingText.active = true;
        this.playBtn.active = false;
        window.api.getRequest(APINAME.DEBIT_VERIFY, { tid: window.tid }, ((result) => {
            // this.loadingText.active = false;
            if (result["status"]["resp_code"] == 200) {
                window.gpid = result["gpid"];
                this.getConfigData();
            } else {
                let data = {
                    "status": {
                        "resp_message": result["status"]["resp_message"],
                        "heading": "WHOOPS!",
                    }

                }
                //data["origin"] = Analytics_Screen.HOME;
                this.handleError(data);
            }
        }));
    },

    PlayBtn: function () {
        if (window.paytm) {
            this.playPaytm();
        } else {
            this.play();
        }

    },


    reStartGame(origin) {
        if (window.gameMode === "Single") {
            this.OnSinglePLayerClick();
        } else
            this.multiplayerGame(origin);
    },


    joinRoom() {
        PopUpManager.show(PopUpType.Loading, {});
        var self = this;
        console.log('avatarId' + window.avatarId);
        let options = {
            "token": window.bearertoken,
            "gid": window.gameId,
            "mid": window.merchantId,
            "gpid": window.gpid,
            "name": window.userName,
            "usertype": window.userType,
            "avatarid": window.avatarId,
            "image": userImageURL,
            "peerid": window.videoEnabled ? window.peer.id : ""
        }

        this.client.joinOrCreate("snakesnladder", options).then(room => {
            self.room = room;
            PopUpManager.hide(PopUpType.Loading);
            window.roomId = self.room.id;
            ScreenManager.showScreen(ScreenEnum.MatchMaking, self.room, function () { });
        }).catch(err => {
            console.log('err' + err);
            PopUpManager.hide(PopUpType.Loading);
            var data = {
                "status": {
                    "resp_message": "Authentication Failed!",
                    "heading": "WHOOPS!"
                }
            }
            self.handleError(data);
        });
    },

    registerLobbyEvents: function () {
        var self = this;
        this.lobby.state.players.onAdd = function (player, sessionId) {
            setTimeout(() => {
                //self.onlinePlayersNo.string = Object.keys(self.lobby.state.players).length + " PLAYERS";
            }, 500);
        }.bind(this);

        this.lobby.state.players.onRemove = function (player, sessionId) {
            setTimeout(() => {
                //self.onlinePlayersNo.string = Object.keys(self.lobby.state.players).length + " PLAYERS";
            }, 500);
        };

        this.lobby.onLeave((code) => {

            var temp = api.getLocalUserData("originalUrl");
            setTimeout(() => {
                console.log(temp);
            }, 1000);

            self.handleLobbyLeaveEvent(code);
        });


    },
    checkBalance() {
        if (!window.showBalance)
            return;
        window.api.getRequest(APINAME.CHECKBALANCE, {}, ((result) => {
            if (result["status"]["resp_code"] == 200) {
                if (result.hasOwnProperty("wallets")) {
                    let walletKeys = Object.keys(result["wallets"]);
                    for (let i = 0; i < walletKeys.length; i++) {
                        this.setBalanceContainer(i, walletKeys[i], result["wallets"][walletKeys[i]]);
                    }
                }
            }
        }).bind(this));
    },

    retry() {
        window.loginToken = "";
        window.bearertoken = "";
        window.tid = "";
        PopUpManager.show(PopUpType.Loading);
        cc.director.loadScene('Game');
    },

    handleLobbyLeaveEvent(code) {
        ;
        console.log("code in handle Lobby Leave Event " + code);
        switch (code) {
            case 1005:
                var data = {
                    "status": {
                        "resp_message": "You have been logged out",
                        "heading": "WHOOPS!",
                        "resp_code": 1005
                    }
                }
                this.handleError(data);
                break;
            case 1006:
                var data = {
                    "status": {
                        "resp_message": "Please make sure you have good network and try again",
                        "heading": "Oops! Internet Problem",
                        "resp_code": 1006
                    }
                }
                this.handleError(data);
                break;
        }
    },

    onMoreGames() {
        this.onButtonClick();
        PopUpManager.show(PopUpType.Exit, {
            leave: () => {
                //window.Analytics.buttonEvent(Action_Name.EXIT_BTN, Analytics_Screen.HOME);
                window.open(window.URLS.MORE_GAMES, "_self");
            }
        });


    },

    openLeaderboard() {
        this.onButtonClick();
        PopUpManager.show(PopUpType.Leaderboard, {});
    },

    howToPlayClick() {
        this.onButtonClick();
        PopUpManager.show(PopUpType.Help, {});
    },

    onAdError() {
        console.log('On Ad Error');
    },

    onAdComplete() {
        console.log('All Ads Completed');
    },

});