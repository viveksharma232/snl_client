module.exports.BroadcastNames = {
    GAME_NOT_FOUND: "GameNotFound",
    START_GAME: "StartGame",
    ON_TURN: "OnTurn",
    ON_TURN_OVER: "OnTurOver",
    ON_DICE_ROLL: "OnDiceRoll",
    ON_DICE_OUTCOME: "OnDiceOutCome",
    GAME_OVER: "GameOver",
    ON_DISCONNECTED: "OnDisconnected",
    ON_RECONNECTED: "OnReconnected",
    ON_PLAYER_LEAVE: "PlayerLeave",
    ON_PEER_CONNECTED: "OnPeerConnected",
    ON_CHAT: "OnChat",
    CHAT: "Chat",
    ON_VIDEOSTATUS_CHANGED: "OnVideoStatuschanged",
    ON_REJECT_CALL: "OnRejectCall",
    REJECTCALL: "RejectCall",
    ON_MAKE_CALL: "ONMakeCall",
    MAKECALL: "MakeCall",
    REWARD : "onReward"
}

module.exports.EventNames = {
    ANSWER: "Answer",
    CHAT: "Chat",
    QUIT: "Quit"
}

module.exports.PlayerStates = {
    IDLE: 0,
    WAITING: 1,
    PLAYING: 2,
    DISCONNECTED: 3,
    OUT: 4,
    DIE: 5
}

module.exports.RoomStates = {
    IDLE: 0,
    WAITING: 1,
    RUNNING: 2,
    OVER: 3
}

module.exports.APINAME = {
    GAMELOGIN: 0,
    CONFIG: 1,
    PLAY: 2,
    DEBIT_VERIFY: 3,
    CHECKBALANCE: 4,
    SAVE_DETAILS: 5,
    LEADERBOARD: 6,
    USER_RANK: 7,
    POSTSCORE: 8,
    USER_DETAILS: 9,
    ANSWER: 10,
    QUESTION: 11,
    CONFIG_WITH_KEY: 12
}

module.exports.ChatType = {
    SELF: 0,
    OTHER: 1
}

exports.Currency = {
    CURRENCY_UNIT_SINGULAR: "Point",
    CURRENCY_UNIT_PLURAL: "Points",
    PREFIX_CURRENCY: false,
}

module.exports.payoutModes = {
    COUPON: "DEFAULT",
    FREE: "NONPAYMENT"
}

exports.Action_Name = {
    SFX_BTN: "sfx",
    MUSIC_BTN: "music",
    COPYANDBOOK_BTN: "copyandbook",
    COPY_BTN: "copy",
    LEAVE_BTN: "leave",
    HOME_BTN: "home",
    REPLAY_BTN: "replay",
    MORE_BTN: "more",
    EXIT_BTN: "exit",

}

exports.App_Name = "snl";

module.exports.COLYSEUS_ERROR_CODE = {
    LOGOUT: {
        MSG: "You have been LoggedOut",
        CODE: 1005
    },
    TOO_LONG_TIME: {
        MSG: "You are playing from too long time",
        CODE: 1006
    }
}

exports.Ad_Events = {
    AD_ERROR: 'onAdError',
    ALL_ADS_COMPLETED: 'onAllAdsCompleted'
}

module.exports.GAME_CONST = {
    MUSIC_STATUS: "snlmusicvolume",
    SFX_STATUS: "snlsfxvolume",
    HELP_REQUIRED: "snlhelp"
}