var PopUpBase = require('PopUpBase');
cc.Class({
    extends: PopUpBase,

    properties: {
        dice: {
            default: null,
            type: cc.Node
        },
    },

    onHomeClick() {
        this.onButtonClick();
        PopUpManager.hideCurrentPopUp();
        ScreenManager.showScreen(ScreenEnum.LoginScreen, null, () => { });
    },

    moreGames() {
        this.onButtonClick();
        window.open(URLS.MORE_GAMES, "_self"); //http://itapuat.infini.work/static/games.html
    },

    onReplayClick() {
        this.onButtonClick();
        PopUpManager.hideCurrentPopUp();
        ScreenManager.showScreen(ScreenEnum.LoginScreen, null, function () { });
        ScreenManager.screens[ScreenEnum.LoginScreen].reStartGame();
    },

    onHide: function (data) {
        this.dice.getComponent(cc.Animation).stop();
        this.dice.getComponent(cc.Sprite).enabled = false;
    },

});