var PopUpBase = require('PopUpBase');

cc.Class({
    extends: PopUpBase,

    properties: {
        speed: {
            default: 100,
            type: cc.Integer
        },
        load: {
            type: cc.Node,
            default: null
        }
    },

    onShow(data) {

    },

    onHide() {

    },

    update(dt) {
        this.load.angle += dt * this.speed;
    },
});