var PopUpBase = require('PopUpBase');

cc.Class({
    extends: PopUpBase,

    properties: {

    },

    onShow: function (cb) {
        this.exitCallBack = cb.leave;
        this.resumeCallBack = cb.resume;
    },

    onExit() {
        this.onButtonClick();
        PopUpManager.hideCurrentPopUp();
        if (this.exitCallBack)
            this.exitCallBack();
    },

    onContinue() {
        this.onButtonClick();
        PopUpManager.hideCurrentPopUp();
        if (this.resumeCallBack)
            this.resumeCallBack();
    }


});