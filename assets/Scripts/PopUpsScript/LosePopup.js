var PopUpBase = require('PopUpBase');
var { Currency } = require('../Constants');
cc.Class({
    extends: PopUpBase,

    properties: {

        dice: {
            default: null,
            type: cc.Node
        },
        gameFeeLbl: {
            default: null,
            type: cc.Label
        }
    },

    onShow: function (data) {
        this.gameFeeLbl.string = gameFee == 0 ? "" : "for " + this.getCurrencyWithUnit(gameFee);
        this.gameFeeLbl.node.y = 0;
    },

    getCurrencyWithUnit(value) {
        let unit = (parseInt(value) > 1 ? Currency.CURRENCY_UNIT_PLURAL : Currency.CURRENCY_UNIT_SINGULAR);
        if (Currency.PREFIX_CURRENCY)
            return (unit + " " + value);
        return (value + " " + unit);
    },

    onHomeClick() {
        this.onButtonClick();
        PopUpManager.hideCurrentPopUp();
        ScreenManager.showScreen(ScreenEnum.LoginScreen, null, () => { });
    },

    onReplayClick() {
        this.onButtonClick();
        PopUpManager.hideCurrentPopUp();
        ScreenManager.showScreen(ScreenEnum.LoginScreen, null, function () { });
        ScreenManager.screens[ScreenEnum.LoginScreen].replayGame();
    },

    onHide: function (data) {
        this.dice.getComponent(cc.Animation).stop();
        this.dice.getComponent(cc.Sprite).enabled = false;
    },
    
    onPlayMoreGames() {
        window.open(URLS.MORE_GAMES, "_self"); //http://itapuat.infini.work/static/games.html
    }

});