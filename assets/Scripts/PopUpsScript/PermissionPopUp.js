var PopUpBase = require('PopUpBase');


cc.Class({
    extends: PopUpBase,

    properties: {
        message: {
            type: cc.Label,
            default: null
        }
    },

    onShow() {
        if (window.localStream === null || window.localStream === undefined) {
            this.message.string = "You have blocked video";
            return;
        }
        this.message.string = "You have given permission for video";
    },

    okBtnClick() {
        this.onButtonClick();
        PopUpManager.hideCurrentPopUp();
    }
});