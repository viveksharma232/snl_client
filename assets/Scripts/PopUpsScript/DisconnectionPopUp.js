var PopUpBase = require('PopUpBase');
// import * as Colyseus from "./Colyseus/colyseus";

cc.Class({
    extends: PopUpBase,

    properties: {
        message: {
            type: cc.Label,
            default: null
        },
        title: {
            type: cc.Label,
            default: null
        }
    },

    onShow: function(data) {},

    onCancelClick() {
        this.onButtonClick();
        PopUpManager.hideCurrentPopUp();
        cc.game.end();
    },

    onRetryClick() {
        this.onButtonClick();
        PopUpManager.hideCurrentPopUp();
        cc.director.loadScene('Game');
    }
});