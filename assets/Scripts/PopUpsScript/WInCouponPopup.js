var PopUpBase = require('PopUpBase');
var { Currency } = require('../Constants');
cc.Class({
    extends: PopUpBase,

    properties: {
        couponLbl: {
            type: cc.Label,
            default: null,
        },
        dice: {
            default: null,
            type: cc.Node
        },
        copyLbl: {
            type: cc.Label,
            default: null,
        }, titleLbl: {
            default: null,
            type: cc.Label
        },
    },

    onShow: function (data) {
        this.copyLbl.string = "Copy";
        this.titleLbl.string = window.reward.coupon.heading;
        this.couponCode();
    },

    getCurrencyWithUnit(value) {
        let unit = (parseInt(value) > 1 ? Currency.CURRENCY_UNIT_PLURAL : Currency.CURRENCY_UNIT_SINGULAR);
        if (Currency.PREFIX_CURRENCY)
            return (unit + " " + value);
        return (value + " " + unit);
    },

    couponCode() {
        var couponCode = !!window.reward && !!window.reward.coupon ? window.reward.coupon.code : WE3474;
        if (!!couponCode) {
            this.couponLbl.string = couponCode;
            this.knowmoreText = window.reward.coupon.knowmore;
        }
    },

    copyCode: function (event, customData) {
        //create input element
        this.onButtonClick();
        var i = document.createElement("input");
        i.type = "text";
        i.name = "user_name";
        i.id = "user_name1";

        i.value = this.couponLbl.string;
        var body = document.getElementsByTagName("BODY")[0];
        body.appendChild(i);

        i.select();

        /* Copy the text inside the text field */
        document.execCommand("copy");

        /* Alert the copied text */
        //alert("Copied to clipboard : " + i.value);

        body.removeChild(i);
        // this.copiedPopup.active = false;
        // this.couponCodeLabel.active = false;

        if (customData == 1) {
            this.directToOlaPage();
        } else {
            this.copyLbl.string = "Copied";
            this.copyLbl.node.active = true;
        }
    },

    directToOlaPage() {
        this.onButtonClick();
        window.location.replace("https://www.olacabs.com/");
    },

    openTermsAndCondition() {
        this.onButtonClick();
        PopUpManager.show(PopUpType.TermsAndCondition, this.knowmoreText);
    },


    directToOlaPage() {
        this.onButtonClick();
        window.location.replace("https://www.olacabs.com/");
    },

    onHomeClick() {
        this.onButtonClick();
        PopUpManager.hideCurrentPopUp();
        ScreenManager.showScreen(ScreenEnum.LoginScreen, null, () => { });
    },

    moreGames() {
        this.onButtonClick();
        window.open(URLS.MORE_GAMES, "_self"); //http://itapuat.infini.work/static/games.html
    },

    onReplayClick() {
        this.onButtonClick();
        PopUpManager.hideCurrentPopUp();
        ScreenManager.showScreen(ScreenEnum.LoginScreen, null, function () { });
        ScreenManager.screens[ScreenEnum.LoginScreen].reStartGame();
    },

    onHide: function (data) {
        this.dice.getComponent(cc.Animation).stop();
        this.dice.getComponent(cc.Sprite).enabled = false;
    },

});