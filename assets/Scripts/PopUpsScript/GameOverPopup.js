var PopUpBase = require('PopUpBase');

cc.Class({
    extends: PopUpBase,

    properties: {
        message: {
            type: cc.Label,
            default: null
        },
        title: {
            type: cc.Label,
            default: null
        }
    },

    onShow: function(data)
    {
        this.setTitleText("Game Over");
        this.setMessageText(data.winnerText);
    },
    setMessageText: function (msg) {
        this.message.string = msg;
    },
    
    setTitleText: function (title) {
        this.title.string = title;
    },
});