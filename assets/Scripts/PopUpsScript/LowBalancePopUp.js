var PopUpBase = require('PopUpBase');

cc.Class({
    extends: PopUpBase,

    properties: {},

    onShow: function(data) {},


    exitClick() {
        this.onButtonClick();
        PopUpManager.hideCurrentPopUp();
    }
});