var PopUpBase = require('PopUpBase');
var { Currency } = require('../Constants');

cc.Class({
    extends: PopUpBase,

    properties: {

        playerIds: {
            default: [],
            type: cc.Label
        },
        players: {
            default: [],
            type: cc.Node
        },
        dice: {
            default: null,
            type: cc.Node
        },
        winAmountLbl: {
            default: null,
            type: cc.Label
        },
        gameFeeLbl: {
            default: null,
            type: cc.Label
        }
    },

    onShow: function (data) {
        console.log(data);
        this.winAmountLbl.string = winAmount == 0 ? '' : "WIN UPTO " + this.getCurrencyWithUnit(winAmount);
        this.gameFeeLbl.string = gameFee == 0 ? "" : "for " + this.getCurrencyWithUnit(gameFee);
        this.gameFeeLbl.node.y = 0;
    },

    getCurrencyWithUnit(value) {
        let unit = (parseInt(value) > 1 ? Currency.CURRENCY_UNIT_PLURAL : Currency.CURRENCY_UNIT_SINGULAR);
        if (Currency.PREFIX_CURRENCY)
            return (unit + " " + value);
        return (value + " " + unit);
    },

    onReplayClick() {
        this.onButtonClick();
        PopUpManager.hideCurrentPopUp();
        ScreenManager.showScreen(ScreenEnum.LoginScreen, null, function () { });
        ScreenManager.screens[ScreenEnum.LoginScreen].replayGame();
    },

    onHide: function (data) {
        this.dice.getComponent(cc.Animation).stop();
        this.dice.getComponent(cc.Sprite).enabled = false;
    },

    onHomeClick() {
        this.onButtonClick();
        PopUpManager.hideCurrentPopUp();
        ScreenManager.showScreen(ScreenEnum.LoginScreen, null, () => { });
    },

    onPlayMoreGames() {
        window.open(URLS.MORE_GAMES, "_self"); //http://itapuat.infini.work/static/games.html
    }

});