var PopUpBase = require('PopUpBase');

cc.Class({
    extends: PopUpBase,

    properties: {
        message: {
            type: cc.Label,
            default: null
        },
        title: {
            type: cc.Label,
            default: null
        }
    },

    onShow: function(data) {
        //  Analytics.viewEvent(Analytics_Screen.ERROR, data["origin"]);
        this.setMessageText(data["status"]["resp_message"]);
        this.setTitleText(data["status"]["heading"]);
        this.code = data["status"]["resp_code"];
    },

    setMessageText: function(msg) {
        this.message.string = msg;
    },

    setTitleText: function(title) {
        this.title.string = title;
    },

    onCancelClick() {
        PopUpManager.hideCurrentPopUp();
    },

    onRetryClick() {
        this.onButtonClick();
        PopUpManager.hideCurrentPopUp();
        console.log("current Screen ius ", ScreenManager.currentScreen);

        ScreenManager.screens[ScreenManager.currentScreen].retry();
    }
});