var PopUpBase = require('PopUpBase');

cc.Class({
    extends: PopUpBase,

    properties: {},

    onShow: function(data) {},

    OnOkClick() {
        this.onButtonClick();
        ScreenManager.screens[ScreenManager.currentScreen].joinLobby(true);
        PopUpManager.hideCurrentPopUp();
    }
});