var PopUpBase = require('PopUpBase');

cc.Class({
    extends: PopUpBase,

    properties: {

    },

    onShow: function () { },

    cancelClick() {
        this.onButtonClick();
        PopUpManager.hideCurrentPopUp();
    },

    BackToGameClick() {
        this.onButtonClick();
        PopUpManager.hideCurrentPopUp();
        console.log("current Screen on BackToGame" + ScreenManager.currentScreen);
        ScreenManager.screens[ScreenManager.currentScreen].OnMultiplayerClick();
    }


});