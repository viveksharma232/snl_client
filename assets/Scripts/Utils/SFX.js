window.GameSFX = new cc.Enum({
    Answer_Correct: 0,
    Answer_Wrong: 1,
    Game_Begin: 2,
    Game_Lost: 3,
    Game_Matched: 4,
    Game_Win: 5,
    Game_Music: 6,
    Timer_Loop: 7,
    UI_ButtonClick: 8,
    UI_PopUp: 9,
    Game_Ladder:10,
    Game_Snake : 11
});