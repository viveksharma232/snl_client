import { APINAME, payoutModes } from "../Constants";
// import Decryptor from "../Encryption/Decryptor";

export default function API() {

}

API.prototype.getRequest = function getRequest(apiName, options, callback) {
    // var decryptor = new Decryptor();
    var request_url = this.getAPIURL(apiName);
    var http = new XMLHttpRequest();
    var self = this;
    http.timeout = 7000; //7 seconds

    var params = '';
    if (options) {

        for (var key in options) {
            params += ((Object.keys(options)[0] == key) ? '?' : '&') + key + '=' + options[key];
        }
    }
    console.log("request url : " + (request_url + params));

    http.open("GET", request_url + params, true);
    if (apiName == APINAME.PLAY || apiName == APINAME.CHECKBALANCE || apiName == APINAME.CONFIG) {
        http.setRequestHeader("authorization", "Bearer " + window.bearertoken);
    }
    http.onreadystatechange = function () {
        if (http.responseText) {
            var responseJSON = eval('(' + http.responseText + ')');
            // var responseJSON = decryptor.decrypt(http.responseText);
        } else {
            var responseJSON = {};
        }
        switch (http.readyState) {
            case 4:
                if (Object.keys(responseJSON).length > 0) {
                    var msg = self.errorCases(responseJSON["status"]["resp_code"]);
                    if (msg != "")
                        responseJSON["status"]["resp_message"] = msg;
                    callback(responseJSON);
                }
        }
    };

    http.ontimeout = function () {
        var errorJson = {
            "status": {
                "resp_code": 405,
                "heading": "Whoops!",
                "resp_message": "Please make sure you have good network and try again"
            }
        }
        callback(errorJson);
    };

    http.onerror = function () {
        var errorJson = {
            "status": {
                "resp_code": 405,
                "heading": "Whoops!",
                "resp_message": "Please make sure you have good network and try again"
            }
        }
        callback(errorJson);
    };

    http.send();
}

API.prototype.postRequest = function getRequest(apiName, postData, callback) {

    var request_url = this.getAPIURL(apiName);
    console.log("%%%%%%%%%%%" + apiName + "  url " + request_url + " postData is ", postData);
    var http = new XMLHttpRequest();
    var self = this;
    http.timeout = 7000; //7 secondds


    http.open("POST", request_url, true);
    http.setRequestHeader("authorization", "Bearer " + window.bearertoken);
    http.setRequestHeader("Content-type", "application/json");

    http.onreadystatechange = function () {
        if (http.responseText) {
            var responseJSON = eval('(' + http.responseText + ')');
            // var responseJSON = decryptor.decrypt(http.responseText);
        } else {
            var responseJSON = {};
        }
        switch (http.readyState) {
            case 4:
                if (Object.keys(responseJSON).length > 0) {
                    var msg = self.errorCases(responseJSON["status"]["resp_code"]);
                    if (msg != "")
                        responseJSON["status"]["resp_message"] = msg;
                    callback(responseJSON);
                }
        }
    };

    http.ontimeout = function () {
        var errorJson = {
            "status": {
                "resp_code": 405,
                "heading": "Whoops!",
                "resp_message": "Please make sure you have good network and try again"
            }
        }
        callback(errorJson);
    };

    http.onerror = function () {
        var errorJson = {
            "status": {
                "resp_code": 405,
                "heading": "Whoops!",
                "resp_message": "Please make sure you have good network and try again"
            }
        }
        callback(errorJson);
    };

    http.send(JSON.stringify(postData));
}

API.prototype.errorCases = function errorCases(code) {
    switch (code) {
        case 401:
            return "Login Failed";
        case 414:
            return "You have already played today\nCome back tomorrow!"
        case 415:
            return "You have already played\nin this season\nCome back again in the next season!"
        case 3001:
        case 3013:
            return "Something Went Wrong";
        case 3002:
        case 3003:
        case 3004:
        case 3006:
        case 3007:
            return "Login Failed";
        case 3005:
        case 416:
            return "Buy or Watch Videos to earn Points" //"Insufficient Funds";
        case 3008:
            return "Reward Code Is Invalid";
        case 1001:
            return "Something Went Wrong";
        case 3011:
        case 3012:
            return "Please Login again";
        case 1200:
        case 1201:
        case 1202:
            return "Looks like you cancelled the payment.";

        default:
            return "";
    }
}

API.prototype.getAPIURL = function getAPIURL(name) {
    switch (name) {
        case APINAME.LOGIN:
            return this.getHostName() + this.getMerchantName() + "login";
        case APINAME.GAMELOGIN:
            return this.getHostName() + this.getMerchantName() + "login/snakesnladders";
        case APINAME.CONFIG:
            return "https://us-central1-commonconfig.cloudfunctions.net/getMerchantDetails";
        case APINAME.PLAY:
            return this.getHostName() + this.getMerchantName() + "play";
        case APINAME.CHECKBALANCE:
            return this.getHostName() + this.getMerchantName() + "balance";
        case APINAME.USER_DETAILS:
            return this.getHostName() + this.getMerchantName() + "user";
        case APINAME.GAME:
            return BIND_DATA.GAMESERVER + "casual/game";
        case APINAME.POSTSCORE:
            return BIND_DATA.GAMESERVER + "casual/postscore";
        case APINAME.SAVE_DETAILS:
            return this.getHostName() + "/auth" + this.getMerchantName() + "update";
        case APINAME.LEADERBOARD:
            return this.getHostName() + "/leaderboard/getleaderboard";
        case APINAME.USER_RANK:
            return this.getHostName() + "/leaderboard/getrank";
        case APINAME.CONFIG_WITH_KEY:
            return "https://us-central1-commonconfig.cloudfunctions.net/getMerchantDetails";

    }
    return name;
}

API.prototype.getMerchantName = function () {
    return MERCHANT_PATH + "/";
}

API.prototype.getHostName = function () {
    //return "https://experientialdeals.com/";
    return "https://" + MERCHANT_HOST;
    //  return "https://mhg.huntergames.live/";
}

API.prototype.getCurrencyWithUnit = (value, walletName) => {
    let currencyWallet = WALLETS[walletName];
    let unit = ((value) > 1 ? currencyWallet.pluralunit : currencyWallet.singularunit);
    if (currencyWallet.prefixcurrency)
        return (unit + " " + value);
    return (value + " " + unit);
}

API.prototype.loadImage = function (imgUrl) {
    return new Promise((resolve, reject) => {
        if (!imgUrl)
            return reject();
        cc.loader.load(imgUrl, (err, tex) => {
            if (err) {
                console.log("Not Able to Download image of url " + imgUrl);
                return reject();
            }
            return resolve(new cc.SpriteFrame(tex));
        });
    })

}

//to get Local Data from inside merchant key
API.prototype.getLocalUserData = function (key) {
    let merchantData = cc.sys.localStorage.getItem(window.MERCHANT_PATH.substring(1));
    if (!merchantData)
        return undefined;
    merchantData = JSON.parse(merchantData);
    return this.jsonOrString(merchantData[key]);
}



//Return json if json other wise return normal string
API.prototype.jsonOrString = function (str) {
    let parseJson = {};
    try {
        parseJson = JSON.parse(str);
    } catch (e) {
        return str;
    }

    return parseJson;
}


//To Set Local Data inside merchantId you should send a json of key and value in single level mode
API.prototype.setLocalUserData = (data) => {
    let merchantUserData = cc.sys.localStorage.getItem(window.MERCHANT_PATH.substring(1));
    if (!merchantUserData)
        merchantUserData = {};
    else
        merchantUserData = JSON.parse(merchantUserData);
    let keys = Object.keys(data);
    for (let i = 0; i < keys.length; i++) {
        merchantUserData[keys[i]] = data[keys[i]];
    }
    cc.sys.localStorage.setItem(window.MERCHANT_PATH.substring(1), JSON.stringify(merchantUserData));
}

// API.prototype.getAPIURLOLD = function getAPIURL(name) {
//     switch (name) {
//         case APINAME.LOGIN:
//             return this.getHostName() + this.getMerchantName() + "login";
//         case APINAME.GAMELOGIN:
//             return this.getHostName() + this.getMerchantName() + "login/snakesnladders"; //
//         case APINAME.CONFIG:
//             return "https://us-central1-commonconfig.cloudfunctions.net/getMerchantDetails3";
//         case APINAME.PLAY:
//             return this.getHostName() + this.getMerchantName() + "play";
//         case APINAME.USER_DETAILS:
//             return this.getHostName() + this.getMerchantName() + "user";
//         case APINAME.CHECKBALANCE:
//             return this.getHostName() + this.getMerchantName() + "balance";
//         case APINAME.GAME:
//             return BIND_DATA.GAMESERVER + "casual/game";
//         case APINAME.POSTSCORE:
//             return BIND_DATA.GAMESERVER + "casual/postscore";
//         case APINAME.SAVE_DETAILS:
//             return this.getHostName() + "/auth" + this.getMerchantName() + "update";
//         case APINAME.LEADERBOARD:
//             return this.getHostName() + "/leaderboard/getleaderboard";
//         case APINAME.USER_RANK:
//             return this.getHostName() + "/leaderboard/getrank";
//     }
//     return name;
// }