
cc.Class({
    extends: cc.Component,

    properties: {

    },

    onEnable: function () {
        var self = this;
        setTimeout(() => {
            self.node.active = false;
        }, 500);
    }
});
