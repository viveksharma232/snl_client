var Emitter = require('EventEmitter');

cc.Class({
    extends: cc.Component,

    properties: {
        onPos:cc.Node,
        offPos:cc.Node,
        status:true,
        target:cc.Node,
        duration:0.3,
        icon:cc.Sprite,
        bg:cc.Sprite,
        onIcon:cc.SpriteFrame,
        offIcon:cc.SpriteFrame,

        onBg:cc.SpriteFrame,
        offBg:cc.SpriteFrame,

        // eventTag:"",
    },

    // onclick
    // move slide
    // check bool and take action
    onClick : function()
    {
        // this.target.stopAllActions();
        if(this.status)
        {
            // var actionTo = cc.moveTo(this.duration, this.offPos.position);
            // this.target.runAction(actionTo);
            this.icon.spriteFrame = this.offIcon;
            this.bg.spriteFrame = this.offBg;
            this.target.position = this.offPos.position;
        }
        else
        {
            // var actionTo = cc.moveTo(this.duration, this.onPos.position);
            // this.target.runAction(actionTo);
            this.icon.spriteFrame = this.onIcon;
            this.bg.spriteFrame = this.onBg;
            this.target.position = this.onPos.position;
 
        }

        this.status = !this.status;

        // emit event with status  
        Emitter.inst.emit(this.node.name, this.status); 
    },

    setDefault:function()
    {
        if(this.status)
        {
            // var actionTo = cc.moveTo(this.duration, this.offPos.position);
            // this.target.runAction(actionTo);
            this.icon.spriteFrame = this.offIcon;
            this.bg.spriteFrame = this.offBg;
            this.target.position = this.offPos.position;
        }
        else
        {
            // var actionTo = cc.moveTo(this.duration, this.onPos.position);
            // this.target.runAction(actionTo);
            this.icon.spriteFrame = this.onIcon;
            this.bg.spriteFrame = this.onBg;
            this.target.position = this.onPos.position;
 
        }

        this.status = !this.status;
    },

});
