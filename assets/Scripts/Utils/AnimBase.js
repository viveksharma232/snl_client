cc.Class({
    extends: cc.Component,

    properties: {
        callback: { default: null, visible: false },
        anim: { default: null, type: cc.Animation },
        reverse: { default: false, visible: false },
        // target:null,
    },

    play: function(msg, cb) {
        // console.log(msg);
        // console.log(cb);

        this.callback = cb;;
        if (msg == "showPopUp") {
            // this.anim.currentClip.speed = 1;
            this.anim.play('popup').wrapMode = cc.WrapMode.Normal;
            this.reverse = false;
        } else {
            console.log("Here");
            // this.anim.currentClip.speed = -1;
            // this.anim.setCurrentTime(0.1);
            this.anim.play('popup').wrapMode = cc.WrapMode.Reverse;
            this.reverse = true;

        }
    },

    onAnimEnd: function() {
        if (!this.reverse) {
            console.log("Anim Callback");
            this.callback();
        }
    },

    OnAnimStart: function() {
        if (this.reverse) {
            console.log("Anim Callback");
            this.callback();
        }
    },

});