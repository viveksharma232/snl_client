cc.Class({
    extends: cc.Component,

    properties: {

    },

    onLoad: function() {

    },

    onShow: function(data) {
        this.data = data;
        PopUpManager.sounds.playSound(GameSFX.UI_PopUp, false);
    },

    onHide: function() {
        PopUpManager.sounds.playSound(GameSFX.UI_PopUp, false);
    },

    onButtonClick: function() {
        PopUpManager.sounds.playSound(GameSFX.UI_ButtonClick, false);
    },

    onDestroy: function() {

    }
});