/**
 * owner nane
 * date of creation
 * company name
 * summary of class
 * Modified: #1 Dharmaraj P - Animation callBack bug fix
 */
var SoundManager = require('../SoundManager');
var PopUpBase = require('PopUpBase');
var root = window;
var PopUpType = new cc.Enum({
    Error: 0,
    Win: 1,
    Lose: 2,
    Exit: 3,
    Disconnect: 4,
    DuplicateLogin: 5,
    No_Opponent: 6,
    Low_Balance: 7,
    Permission: 8,
    Loading: 9,
    Help: 10,
    //Leaderboard: 11,
    WinCoupon: 11,
    LoseCoupon: 12,
    TermsAndCondition: 13,
    None: 100,
});

/**
 * PopUp Manager Class
 */
var PopUpManager = cc.Class({
    extends: cc.Component,

    properties: {
        popUps: {
            default: [],
            type: PopUpBase,
        },
        currentPopUp: {
            default: PopUpType.None,
            type: PopUpType
        },
        currentOverLayedPopUps: {
            default: [],
            type: PopUpType,
        },
        sounds: {
            default: null,
            type: SoundManager
        }
    },

    onLoad: function () {
        root.PopUpManager = this;
        this.registerCloseBtn();
        root.PopUpType = PopUpType;
        //   this.currentOverLayedPopUps.push(PopUpType.None);
    },
    registerCloseBtn: function () {
        var inst = this;
        cc.systemEvent.on(cc.systemEvent.KEY_UP, function (e) {
            // console.log(kcode);
            if (kcode == cc.macro.KEY.back || kcode == cc.macro.KEY.escape) {
                if (inst.currentOverLayedPopUps.length > 0) {
                    e.stopPropagation();
                }
                inst.hideCurrentPopUp();
            }
        }, this.node);
    },

    /**
     * Show Pop up
     */
    show: function (popUp, data = null, callback = null) {
        var isOpen = (this.currentOverLayedPopUps.indexOf(popUp) === -1) ? false : true;

        if (popUp !== null && popUp !== PopUpType.None && !isOpen) {
            this.currentOverLayedPopUps.push(popUp);
            // console.log("After push isOpen" + isOpen);

            this.popUps[popUp].node.active = true;
            var anim = this.popUps[popUp].getComponent('AnimBase');
            var inst = this;
            if (anim !== null) {
                anim.play("showPopUp", function () {
                    console.log("PopUP shown");
                    inst.popUps[popUp].onShow(data);

                    if (callback !== null)
                        callback();
                });
            } else {
                this.popUps[popUp].onShow(data);
                // if (ScreenManager.currentScreen == ScreenEnum.Gameplay) {
                //     //   document.getElementById("togglegame").style.zIndex = -1;
                //     console.error(" videoPanel current ", window.VideoChatManager.videopanel);
                //     if (window.VideoChatManager.videopanel !== null && window.VideoChatManager.videopanel.classList.contains("videopanelactive"))
                //         window.VideoChatManager.superToggle(window.VideoChatManager.videopanel, "videopanelactive", "videopaneldeactive");
                // }
                if (callback !== null)
                    callback();
            }

            this.currentPopUp = popUp;
        }
    },

    /**
     * Hide PopUp
     */
    hide: function (popUp, callBack = null) {
        //   console.log("hide popup ...." + popUp + "   current popup list " + this.currentOverLayedPopUps.length)
        var instance = this;
        var isOpen = (this.currentOverLayedPopUps.indexOf(popUp) == -1) ? false : true;
        //  console.log("isOpen " + isOpen);
        if (popUp !== null && popUp !== PopUpType.None && isOpen) {
            //  console.log("=========="+this.currentOverLayedPopUps.pop());
            this.currentOverLayedPopUps.splice(this.currentOverLayedPopUps.indexOf(popUp), 1);
            var anim = this.popUps[popUp].getComponent('AnimBase');
            var inst = this;
            if (anim !== null) {
                anim.play("hidePopUp", function () {
                    console.log("PopUp Closed");
                    inst.popUps[popUp].node.active = false;
                    inst.popUps[popUp].onHide();
                    if (callBack !== null)
                        callBack();
                });
            } else {
                //     console.log('anim is null');
                this.popUps[popUp].onHide();

                this.popUps[popUp].node.active = false;
                if (callBack !== null)
                    callBack();
            }
            if (ScreenManager.currentScreen == ScreenEnum.Gameplay) {
                //  document.getElementById("togglegame").style.zIndex = 2;
            }
            //   this.popUps[popUp].node.active = false;
            this.currentPopUp = this.currentOverLayedPopUps[this.currentOverLayedPopUps.length - 1];

        } else {
            if (callBack !== null)
                callBack();
        }
    },

    /**
     * Hide current PopUp
     */
    hideCurrentPopUp: function (callBack = null) {
        // if (this.currentPopUp == 5) {
        //     this.hide(this.currentPopUp, function() {
        //         this.show(4, null, function() {});
        //     }.bind(this));
        // } else {
        this.hide(this.currentPopUp, callBack);
        // }
    },

    /**
     * Hide all Popups
     */
    hideAllPopUps: function () {
        //    console.log("current popups = " + this.currentOverLayedPopUps.length);
        while (this.currentOverLayedPopUps.length !== 0) {
            this.hide(this.currentPopUp, function () { });
        }
    }
});

module.exports = {
    PopUpManager: PopUpManager,
    PopUpType: PopUpType
}