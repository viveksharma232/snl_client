// for handling tab functionalities - later use

cc.Class({
    extends: cc.Component,

    properties: {
        tabs: {
            default: [],
            type: cc.Node,
        },
        tabButtons: {
            default: [],
            type: cc.Sprite,
        },
        currentTab: {
            default: 0,
            // visible: false,
            // type: cc.Node,
        },
        // activeSprite: {
        //     default: null,
        //     type: cc.SpriteFrame,
        // },
        // inactiveSprite: {
        //     default: null,
        //     type: cc.SpriteFrame,
        // },
        activeColor: {
            default: new cc.Color(),
            // type: cc.Color,
        },
        inactiveColor: {
            default: new cc.Color(),
            // type: cc.Color,
        },

    },

    // use this for initialization
    onLoad: function () {

    },

    start: function () {

    },

    /**
     * Updates the current table content
     */
    updateCurrentTable: function () {

        this.tabs[this.currentTab].getComponent('Table').onEnable();
        // console.log("UpdateTable!");
    },

    /**
     * Handles tab transition
     */
    setActiveTab: function (currTab, prevTab) {
        if (prevTab !== null)//&& prevTab !== 'undefined')
        {
            prevTab.active = false;
        }
        currTab.active = true;
    },

    /**
     * Handles tab button transition
     */
    setActiveButton: function (currBtn, prevBtn) {
        if (prevBtn !== null)// && prevBtn !== 'undefined')
        {
            // prevBtn.getComponent(cc.Sprite).spriteFrame = this.inactiveSprite;
            prevBtn.getComponent(cc.Sprite).node.color = new cc.Color(0, 0, 0);
            prevBtn.getComponent(cc.Sprite).node.opacity = 140;
            prevBtn.node.parent.getChildByName("lbl").color = this.inactiveColor;
        }
        currBtn.getComponent(cc.Sprite).node.color = new cc.Color(253, 253, 0);
        currBtn.getComponent(cc.Sprite).node.opacity = 255;
        currBtn.node.parent.getChildByName("lbl").color = this.activeColor;
    },

    /**
     * Show Tab_01 button handler
     */
    onShowTab_01: function () {
        this.setActiveTab(this.tabs[0], this.tabs[this.currentTab]);
        this.setActiveButton(this.tabButtons[0], this.tabButtons[this.currentTab]);
        this.currentTab = 0;
    },

    /**
     * Show Tab_02 button handler
     */
    onShowTab_02: function () {
        this.setActiveTab(this.tabs[1], this.tabs[this.currentTab]);
        this.setActiveButton(this.tabButtons[1], this.tabButtons[this.currentTab]);
        this.currentTab = 1;
    },

    /**
     * Show Tab_03 button handler
     */
    onShowTab_03: function () {
        this.setActiveTab(this.tabs[2], this.tabs[this.currentTab]);
        this.setActiveButton(this.tabButtons[2], this.tabButtons[this.currentTab]);
        this.currentTab = 2;
    },

    /**
     * Show Tab_04 button handler
     */
    onShowTab_04: function () {
        this.setActiveTab(this.tabs[3], this.tabs[this.currentTab]);
        this.setActiveButton(this.tabButtons[3], this.tabButtons[this.currentTab]);
        this.currentTab = 3;
    },

    /**
     * Show Tab_05 button handler
     */
    onShowTab_05: function () {
        this.setActiveTab(this.tabs[4], this.tabs[this.currentTab]);
        this.setActiveButton(this.tabButtons[4], this.tabButtons[this.currentTab]);
        this.currentTab = 4;
    },

    /**
     * Show Tab_06 button handler
     */
    onShowTab_06: function () {
        if (this.tabs.length < 6) {
            return;
        }
        this.setActiveTab(this.tabs[5], this.tabs[this.currentTab]);
        this.setActiveButton(this.tabButtons[5], this.tabButtons[this.currentTab]);
        this.currentTab = 5;
    },



});
