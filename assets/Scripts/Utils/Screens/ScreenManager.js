var AbstractScreen = require('AbstractScreen');
var SoundManager = require('../SoundManager');
var root = window;

var ScreenEnum = new cc.Enum({
    LoginScreen: 0,
    MatchMaking: 1,
    Gameplay: 2,
    None: 100,
});
var ScreenState = new cc.Enum({
    SHOWN: 0,
    HIDDEN: 1,
});

var ScreenManager = cc.Class({
    extends: cc.Component,

    properties: {
        screens: {
            default: [],
            type: [AbstractScreen],
        },

        currentScreen: {
            default: ScreenEnum.LoginScreen,
            type: ScreenEnum,
        },

        screenState: {
            default: null,
            type: ScreenState,
            visible: false,
        },

        sounds: {
            default: null,
            type: SoundManager
        }
    },

    // use this for initialization
    onLoad: function () {
        root.ScreenManager = this;
        ScreenManager.instance = this;
        root.ScreenEnum = ScreenEnum;
        this.registerCloseBtn();
    },

    registerCloseBtn: function () {
        var inst = this;
        cc.systemEvent.on(cc.systemEvent.KEY_UP, function (e) {
            // console.log(kcode);
            if (kcode == cc.macro.KEY.back || kcode == cc.macro.KEY.escape) {
                if (inst.currentScreen == ScreenEnum.SinglePlayerScreen) {
                    inst.hideCurrentScreen(ScreenEnum.MainMenuScreen, null, function () { });
                }
                else if (inst.currentScreen == ScreenEnum.MultiplayerScreen) {
                    inst.hideCurrentScreen(ScreenEnum.MainMenuScreen, null, function () { });
                }
                else if (inst.currentScreen == ScreenEnum.GameScreen) {
                    //   inst.hideCurrentScreen(ScreenEnum.SinglePlayerScreen, null, function () { });
                    //     inst.screens[ScreenEnum.GameScreen].onBackButtonClick();
                }
                else if ((inst.currentScreen == ScreenEnum.MainMenuScreen || inst.currentScreen == ScreenEnum.LoginScreen)) {
                    PopUpManager.show(27, null, function () { });
                }
            }
        }, this);
    },


    showScreen: function (screen, optionalData, callBack) {
        this.enableNewScreen(screen, optionalData, callBack);
    },

    enableNewScreen: function (screen, optionalData, callBack) {
        if (this.currentScreen !== ScreenEnum.None) {
            this.hideCurrentScreen(screen, optionalData, callBack);
        }
        else {
            this.showNextScreen(screen, optionalData, callBack);
        }
    },

    showNextScreen: function (screen, optionalData, callBack) {
        // console.log(screen);
        // console.log(this.screens[screen]);
        this.screens[screen].node.active = true;
        this.currentScreen = screen;
        // var anim = this.screens[screen].getComponent('AnimBase');
        // if (anim === null) {
        this.screens[screen].onShow(optionalData);
        if (callBack !== null)
            callBack();
        this.screenState = ScreenState.SHOWN;
        return;
        // }
        // var inst = this;
        // anim.play("showScreen", function () {
        //     inst.screens[screen].onShow(optionalData);
        //     if (callBack !== null)
        //         callBack();
        // });
        // this.screenState = ScreenState.SHOWN;
    },

    hideCurrentScreen: function (nextScreen, nextOptionalData, nextCallBack) {
        // console.log(nextScreen);
        //var anim = this.screens[this.currentScreen].getComponent('AnimBase');
        var inst = this;
        //if (anim === null) {
        inst.screens[inst.currentScreen].onHide();
        inst.screens[inst.currentScreen].node.active = false;
        inst.screenState = ScreenState.HIDDEN;
        inst.currentScreen = ScreenEnum.None;

        if (nextScreen !== null && nextScreen !== 'undefined') {
            inst.showNextScreen(nextScreen, nextOptionalData, nextCallBack);
        }
        return;
        //}
        // anim.play("hidePopUp", function () {
        //     inst.screens[inst.currentScreen].onHide();
        //     inst.screens[inst.currentScreen].node.active = false;
        //     inst.screenState = ScreenState.HIDDEN;
        //     inst.currentScreen = K.ScreenEnum.None;

        //     if (nextScreen !== null && nextScreen !== 'undefined') {
        //         console.log("NextScreen");
        //         inst.showNextScreen(nextScreen, nextOptionalData, nextCallBack);
        //     }
        // });
    },

    // called every frame, uncomment this function to activate update callback
    // update: function (dt) {

    // },
});
module.exports = {
    ScreenManager: ScreenManager,
    ScreenEnum: ScreenEnum
}