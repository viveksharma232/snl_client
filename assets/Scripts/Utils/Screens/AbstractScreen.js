cc.Class({
    extends: cc.Component,
    properties: {

    },
    // use this for initialization
    onLoad: function() {},

    onShow: function(data) {
        this.data = data;
    },

    onHide: function() {},

    onButtonClick: function() {
        ScreenManager.sounds.playSound(GameSFX.UI_ButtonClick, false);
    },

    onDestroy: function() {}
});