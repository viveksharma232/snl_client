cc.Class({
    extends: cc.Component,

    properties: {
       
        count:{default:0, visible:false},
        duration:0.1,
        animScale:1.1,
        defaultScale:1,
        callback:{
            default: [],
            type: cc.Component.EventHandler,
        },
        anim:{default:null, visible:false}
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start :function() {
        // var self = this;
        this.node.on(cc.Node.EventType.TOUCH_START,function(){
            this.touchStart();
        }, this);

        this.node.on(cc.Node.EventType.TOUCH_END,function(){
            this.touchEnd();
        }, this);

        this.node.on(cc.Node.EventType.TOUCH_CANCEL,function(){
            this.touchEnd();
        }, this);
    },

    onDisable:function()
    {
        this.node.targetOff(this.node);
    },

    touchStart:function()
    {
        // cc.log("TOUCH_START");
        // cc.log(this.count);

        for (this.count = 0; this.count < this.callback.length; this.count++) {
            this.callback[this.count].emit();                    
        }

        // anim
        this.node.stopAllActions();
        this.anim = cc.scaleTo(this.duration,this.animScale);
        this.node.runAction(this.anim);
    },

    touchEnd:function()
    {
        this.node.stopAllActions();
        this.anim = cc.scaleTo(this.duration,this.defaultScale);
        this.node.runAction(this.anim);

    },

    

    // update (dt) {},
});
