var Emitter = require('EventEmitter');

cc.Class({
    extends: cc.Component,

    properties: {
        callback:{
            default: [],
            type: cc.Component.EventHandler,
        },
        targetA:"RunCollider",
        targetB:"SliderCollider",
        index:0,
        triggerOnce:true, // TODO: implement later
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    onLoad:function()
    {
        Emitter.inst.on('start', this.onGameStart, this);
    },

    onDestroy:function()
    {
        Emitter.inst.removeListener('start', this.onGameStart, this);
    },

    onGameStart:function()
    {
        this.node.active = true;
    },

    onCollisionEnter: function (other, self) {
        // console.log("on collision enter");
        // cc.log(other.node.parent.name);
        // cc.log(this.callback);
        // cc.log(this.callback.count);
        // if(other.node.name == this.targetA || other.node.name == this.targetB){
            // stop actions
            // this.node.dispatchEvent( new cc.Event.EventCustom('collision', true) );
            this.node.active = false; 
            if(this.callback.length > 1)
            {
                for (this.index = 0; this.index < this.callback.length; this.index++) {
                    this.callback[index].emit();                    
                }
            }     
            else if(this.callback.length == 1)
            {
                this.callback[0].emit(); 
            }      
        // }        
    },
});
