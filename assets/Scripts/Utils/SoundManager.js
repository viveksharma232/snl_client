var root = window;
cc.Class({
    extends: cc.Component,

    properties: {
        soundList: {
            default: [],
            type: cc.AudioClip
        },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function() {
        root.SoundManager = this;
        SoundManager.instance = this;
        this.audioSource = this.getComponent(cc.AudioSource);
    },

    muteSound: function(flag) {
        this.audioSource.mute = flag;
    },

    playSound: function(id, loop) {
        if (this.audioSource.mute)
            return;
        this.audioSource.clip = this.soundList[id];
        this.audioSource.loop = loop;
        this.audioSource.play();
    },

    stopSound: function() {
        this.audioSource.stop();
    }
});