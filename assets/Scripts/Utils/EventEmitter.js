var EventEmitter = function () {
    this.events = {};
    this.hosts = {};
};

EventEmitter.inst = new EventEmitter();

EventEmitter.prototype.on = function (event, listener, host) {
    if (typeof this.events[event] !== 'object') {
        this.events[event] = [];
        this.hosts[event] = [];
    }

    this.events[event].push(listener);
    this.hosts[event].push(host);
};

EventEmitter.prototype.removeAllListener =  function(){
    this.events = {};
    this.hosts = {};
}

EventEmitter.prototype.removeListener = function (event, listener, host) {
    var idx;
    // cc.log(this.events);
    // cc.log(this.hosts);
    if (typeof this.events[event] === 'object' && typeof this.hosts[event] === 'object') {
        idx = this.events[event].indexOf(listener);
        // id = Array.indexOf(this.hosts[event], host);
        // cc.log(idx);
        // cc.log(this.events[event].length);

        if (idx > -1) {
            this.events[event].splice(idx, 1);
            this.hosts[event].splice(idx, 1);
        }
        // cc.log(this.events[event].length);

    }
};

EventEmitter.prototype.emit = function (event) {
    var i, hosts, listeners, length, args = [].slice.call(arguments, 1);

    if (typeof this.events[event] === 'object') {
        listeners = this.events[event].slice();
        hosts = this.hosts[event].slice();
        length = listeners.length;

        for (i = 0; i < length; i++) {
            if(this.events[listeners[i]] !== null)
            {
                listeners[i].apply(hosts[i], args);
            }
            else
            {
                listeners[i].apply(this, args);
            }
        }
    }
};

EventEmitter.prototype.once = function (event, listener) {
    this.on(event, function g () {
        this.removeListener(event, g);
        listener.apply(this, arguments);
    });
};


module.exports = EventEmitter;
