var Constants = require('./PeerConstants');

const STATE = {
    MAKE: 0,
    OPEN: 1,
    RECEIVE: 2,
    CONNECT: 3,
    DISCONNECT: 4,
    NONE: 5
}

export default function PeerNetwork(cameraId, peerid) {
    console.error("new Peer Netowrk created " + cameraId + " peeris " + peerid);
    this.callRef = this.onCallRequest.bind(this);
    window.peer.on('call', this.callRef);
    this.state = STATE.OPEN;
    this.peerId = peerid;
    this.cameraId = cameraId;
    this.showVideo = true;
    this.stream = null;
    this.consented = false;
    this.videoManager = window.VideoChatManager;
    this.connRef = this.onConnection.bind(this);
    this.videoPanelVideoBtn = document.getElementById("peer" + this.cameraId + "-video");
    this.videoPanelMicroBtn = document.getElementById("peer" + this.cameraId + "-microphon");
    window.peer.on('connection', this.connRef);
}

PeerNetwork.prototype.destroy = function destory() {
    window.peer.off('call', this.callRef);
    window.peer.off('connection', this.connRef);
}

PeerNetwork.prototype.changeCameraID = function changeCameraID(newId) {
    this.cameraId = newId;
    //console.log("camera id changed to " + this.cameraId)
}

PeerNetwork.prototype.startNetwork = function startNetwork(userName) {
    //var delayInMilliseconds = 1; //1 second
    this.makeConnection(userName);
    // setTimeout(function() {
    // }.bind(this), delayInMilliseconds);
}


//on Connection Request from other
PeerNetwork.prototype.onConnection = function onConnection(conn) {
    console.log(" on Connection request i got state is " + this.state);

    console.log("peer network peer id " + this.peerId + " conn peer id " + conn.peer);

    console.log("conectionddddddddddddddddddd ", conn);
    if (this.peerId !== conn.peer) {
        console.log("Wrong Listen " + this.peerId);
        return;
    }
    if (this.state !== STATE.OPEN && this.state !== STATE.MAKE) {
        console.log("not connected as State is " + this.state);
        return;
    }
    this.connection = conn;



    //console.log("peer connection for peer id " + this.peerId);
    if (this.state === STATE.OPEN) {
        this.state = STATE.RECEIVE;
        this.videoManager.eventEmitter.emit(Constants.VIDEOCALL_EVENTS.ON_CONNECTION_REQUEST, conn);
        conn.on("close", (() => {
            console.error("connection loseeeeeeeeeee in lambda ");
            this.state = STATE.OPEN;
            if (!this.consented)
                this.videoManager.eventEmitter.emit(Constants.VIDEOCALL_EVENTS.ON_PEER_DISCONNECT, this.cameraId, conn.peer);
        }).bind(this));

    } else if (this.state === STATE.MAKE) {
        this.state = STATE.CONNECT;
        this.videoManager.eventEmitter.emit(Constants.VIDEOCALL_EVENTS.ON_CONNECT, this.cameraId, true, conn.peer);
        // if (cc.sys.isMobile)
        //     this.onCall();
    }
}

PeerNetwork.prototype.isConnected = function isConnected() {
    return this.state === STATE.CONNECT
}

//Call request from other user
PeerNetwork.prototype.onCallRequest = function onCallRequest(call) {
    console.log("onCallRequest called ", call);
    if (this.peerId !== call.peer) {
        console.log("Wrong call " + this.peerId);
        return;
    }
    this.call = call;
    call.answer(window.localStream, { type: "ANSWER" });
    call.on('stream', (function(stream) {
        console.log("stream.id " + stream.id);
        this.stream = stream;
        this.onReceiveStream(stream, 'peer' + this.cameraId + '-camera');
    }).bind(this));
    call.on('close', (function() {
        this.stream = null;
        this.onReceiveStream(null, 'peer' + this.cameraId + '-camera');
        console.log("video call finised");
    }).bind(this));
}




PeerNetwork.prototype.onCall = function onCall() {
    console.log("window localstream ", window.localStream);
    if (window.localStream === null)
        return;
    var call = window.peer.call(this.peerId, window.localStream, { type: "OFFER" });
    this.call = call;
    //console.log("call " + call);
    call.on('stream', (function(stream) {
        this.stream = stream;
        this.onReceiveStream(stream, 'peer' + this.cameraId + '-camera');
    }).bind(this));
    call.on('close', (function() {
        this.onReceiveStream(null, 'peer' + this.cameraId + '-camera');
        console.log("video call finised");
    }).bind(this));
}

PeerNetwork.prototype.stopPeerAudio = function() {
    if (this.stream === null)
        return;
    this.stream.getAudioTracks()[0].enabled = false;
    console.error("STOP PEEER AUDIO CALLED ");
    this.onReceiveStream(this.stream, 'peer' + this.cameraId + '-camera', true);
    this.videoChatPanelMicroBtn(false);
}

PeerNetwork.prototype.resumePeerAudio = function() {
    console.error("RESUME PEEER AUDIO CALLED " + this.stream + " value " + this.videoPanelMicroBtn.classList.contains("microphoneon"));
    if (this.stream === null)
        return;
    this.stream.getAudioTracks()[0].enabled = this.videoPanelMicroBtn.classList.contains("microphoneon");
    this.onReceiveStream(this.stream, 'peer' + this.cameraId + '-camera', true);
    this.videoChatPanelMicroBtn(true);
}

PeerNetwork.prototype.stopPeerVideo = function() {
    if (this.stream === null)
        return;
    console.log("stopPeerVideo called", this.showVideo);
    this.stream.getVideoTracks()[0].enabled = false;
    this.onReceiveStream(this.stream, 'peer' + this.cameraId + '-camera', true);
    this.videoChatPanelVideoBtn(false);
}

PeerNetwork.prototype.resumePeerVideo = function() {
    if (this.stream === null)
        return;
    console.log("resumePeerVideo called", this.showVideo);
    this.stream.getVideoTracks()[0].enabled = this.videoPanelVideoBtn.classList.contains("videoon");
    this.onReceiveStream(this.stream, 'peer' + this.cameraId + '-camera', true);
    this.videoChatPanelVideoBtn(true);
}

/**
 * On click the connect button, initialize connection with peer
 */
PeerNetwork.prototype.makeConnection = function makeConnection(userName) {
    console.log("onMakeConnection called " + this.peerId);
    console.log("makeConnection ENTER state " + this.state);

    if (this.peerId) {
        let conn = window.peer.connect(this.peerId, {
            metadata: {
                'username': userName
            }
        });
        if (this.state === STATE.RECEIVE) {
            this.state = STATE.CONNECT;
            this.videoManager.eventEmitter.emit(Constants.VIDEOCALL_EVENTS.ON_CONNECT, this.cameraId, false, this.peerId);
            console.log("CONNECT SET FROM MAKE CONEDCTIO M");

            return;
        }
        this.state = STATE.MAKE;

        conn.on("close", (() => {
            console.error("makeConnection loseeeeeeeeeee in lambda " + this.consented);
            this.state = STATE.OPEN;
            if (!this.consented)
                this.videoManager.eventEmitter.emit(Constants.VIDEOCALL_EVENTS.ON_PEER_DISCONNECT, this.cameraId, this.peerId);
        }).bind(this));
        console.log("makeConnection EXIT state " + this.state);
    } else {
        //alert("You need to provide a peer to connect with !");
        return false;
    }
}

PeerNetwork.prototype.onReceiveStream = function onReceiveStream(stream, element_id, disable = false) {
    if (!this.showVideo && !disable)
        return;
    console.log("elemtn id set with mycamera " + element_id + " stream " + stream);
    var video = document.getElementById(element_id);
    video.srcObject = stream;
    if (stream !== null)
        this.videoManager.eventEmitter.emit(Constants.VIDEOCALL_EVENTS.ON_STEAM_STATUS_CHANGED, stream.getVideoTracks()[0].enabled);
}

PeerNetwork.prototype.closeConnection = function closeConnection(ongameOver) {
    this.consented = true;
    if (this.call) {
        this.call.close();
    }
    if (this.connection)
        this.connection.close();

    console.error("GAMEOVEEEEEEEEEEEEEEEEEEEE " + ongameOver);
    if (ongameOver)
        this.destroy();
}

PeerNetwork.prototype.videoChatPanelVideoBtn = function(on) {
    if (on)
        this.videoPanelVideoBtn.classList.remove("hidden");
    else
        this.videoPanelVideoBtn.classList.add("hidden");
}

PeerNetwork.prototype.videoChatPanelMicroBtn = function(on) {
    if (on)
        this.videoPanelMicroBtn.classList.remove("hidden");
    else
        this.videoPanelMicroBtn.classList.add("hidden");

}

PeerNetwork.prototype.enableVideoStream = function() {
    if (this.stream === null)
        return;
    this.stream.getVideoTracks()[0].enabled = this.videoPanelVideoBtn.classList.contains("videoon");
    this.onReceiveStream(this.stream, 'peer' + this.cameraId + '-camera', true);
}

PeerNetwork.prototype.enableAudioStream = function() {
    if (this.stream === null)
        return;
    this.stream.getAudioTracks()[0].enabled = this.videoPanelMicroBtn.classList.contains("microphoneon");
    this.onReceiveStream(this.stream, 'peer' + this.cameraId + '-camera', true);
}