#Video Chat Integration Guide
# //My Peer id is saved in  window.peer.id;

1.Add both in index.html
 <link rel="stylesheet" type="text/css" href="speaker.css" />
  <link rel="stylesheet" type="text/css" href="playpause.css" /> 

  2.Add "gamepanelactive" class on canvas html tag 

  3. Add inside body but above 4th step
  <script src="https://cdn.jsdelivr.net/npm/peerjs@0.3.20/dist/peer.min.js"></script>

  4. Below 3rd Step add These 

<div>
    <button class="togglebtn" id="togglegame">Toggle</button>
  </div>
   <div class="videocontainer videopaneldeactive" id="videocontainer">
    <div id="my" class="videoitem">
      <video id="my-camera" class="video" autoplay="autoplay" muted="true" controls="">
      </video>
    </div>

    <div id="peer1" class="videoitem">
      <video id="peer1-camera" class="video" autoplay="autoplay"></video>
    </div>

    <div id="peer2" class="videoitem">
      <video id="peer2-camera" class="video" autoplay="autoplay"></video>
    </div>

    <div id="peer3" class="videoitem">
      <video id="peer3-camera" class="video" autoplay="autoplay"></video>
    </div>

    <div id="peer4" class="videoitem">
      <video id="peer4-camera" class="video" autoplay="autoplay"></video>
    </div>

    <div id="peer5" class="videoitem">
      <video id="peer5-camera" class="video" autoplay="autoplay"></video>
    </div>
  </div>

  <div class="buttonitems videopaneldeactive" id="videobtns">
    <div class="speakerparent">
      <a class="speaker speakerbtn" id='mute-button'>
        <span></span>
      </a>
    </div>
    <div class="playparent">
      <a id="ppbtn" class="play playbtn active"></a>
    </div>
  </div>


  <script src="script.js"></script>

  5. uncomment script.js class and put it in build-templates

  6.Now copy Video Chat folder without extrafiles folder in your project assets folder

  !Done

