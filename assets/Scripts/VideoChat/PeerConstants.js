exports.VIDEOCALL_EVENTS = {
    ON_OPEN: "onopen",
    ON_CONNECTION_REQUEST: "onrequest",
    ON_CONNECT: "onconnect",
    ON_PEER_DISCONNECT: "onpeerdisconnect",
    MICROPHONE_STATUS: "microphonestatus",
    INTERNAL_VIDEO_STATUS: "videostatus",
    ON_STEAM_STATUS_CHANGED: "onsteamstatus",
    ON_PERMISSION_GIVEN: "onpermission",
    ON_MAKE_CALL: "onmakecall"
}

exports.PEER = {
    HOST_NAME: "experientialdeals.com",
    PORT: 9000,
    STUN_SERVER: {
        url: 'stun:stun1.l.google.com:19302'
    },
    TURN_SERVER: {
        // url: 'turn:numb.viagenie.ca',
        // credential: 'vivek@pass',
        // username: 'vvksharma232@gmail.com'

        url: 'turn:numb.viagenie.ca',
        credential: 'muazkh',
        username: 'webrtc@live.com'

    }
}