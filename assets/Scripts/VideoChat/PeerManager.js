const events = require('events');
var Constants = require('./PeerConstants');
const PeerNetwork = require("./PeerNetwork");

cc.Class({
    extends: cc.Component,

    properties: {
        debugging: true,
        userName: "",
        maxClients: 5 //This is corresponding to how many cameras are there in video
    },

    onLoad() {
        //Colyseus Client should be connected and need to call ColyseusMessage Method when receive a message from room
        window.VideoChatManager = this;
        this.busyCameras = [];
        for (let i = 0; i < this.maxClients; i++)
            this.busyCameras.push(false);
        this.eventEmitter = new events.EventEmitter();
        this.gameCanvas = document.getElementById("GameCanvas");
        this.videopanel = document.getElementById("videocontainer");
        this.closeBtn = document.getElementById("togglegame");

        // this.eventEmitter.on(Constants.VIDEOCALL_EVENTS.ON_OPEN, this.onOpen.bind(this));
        this.eventEmitter.on(Constants.VIDEOCALL_EVENTS.ON_CONNECTION_REQUEST, this.onConnectionRequest.bind(this));
        //this.eventEmitter.on(Constants.VIDEOCALL_EVENTS.ON_RESPONSETOCONNECTION, this.onConnectionResponse.bind(this));
        this.eventEmitter.on(Constants.VIDEOCALL_EVENTS.ON_CONNECT, this.onConnection.bind(this));
        // this.eventEmitter.on(Constants.VIDEOCALL_EVENTS.ON_PEER_DISCONNECT, this.onDisconnect.bind(this));
        this.eventEmitter.on(Constants.VIDEOCALL_EVENTS.ON_STEAM_STATUS_CHANGED, this.onSteamStatusChanged.bind(this));


        this.myPeerId = "";
        this.userName = "";
        this.room = null;
        this.peerNetworks = {};
    },


    firePermission(isAllow) {
        this.eventEmitter.emit(Constants.VIDEOCALL_EVENTS.ON_PERMISSION_GIVEN, isAllow);
    },

    // onDisconnect(cameraId, peerId) {
    //     console.log("on disconnect with cameraId " + cameraId);
    // },
    onConnection(cameraId) {
        // let mediaPlayer = document.getElementById("peer" + cameraId + "-camera");
        // mediaPlayer.muted = true;
        this.onReceiveStream(window.localStream, 'my-camera');
        console.log("success fully connected with peerid ");
    },

    superToggle(element, class0, class1) {
        console.log("inside Peer Manager  toggle called for element ", element + " with class " + element.classList[0] + " second " + element.classList[1]);
        if (class0 !== "")
            element.classList.toggle(class0);
        if (class1 !== "")
            element.classList.toggle(class1);
    },


    initializePeer(stunServer, turnServer) {
        // console.error("initializePeer called with " + window.localStream);

        // if (window.localStream === null || window.localStream === undefined) {
        //     window.peer = {};
        //     window.peer.id = ""
        //     console.error("No Need Of Peer Connection as there is no media stream");
        //     return;
        // }
        window.peer = new Peer({
            host: Constants.PEER.HOST_NAME, //"192.168.100.67",
            // port: Constants.PEER.PORT,
            path: '/snakesnladders/peer',
            secure: true,
            debug: this.debugging ? 3 : 0,
            config: {
                'iceServers': [
                    stunServer,
                    turnServer
                ]
            }
        });

        window.peer.on('open', this.onOpen.bind(this));
        window.peer.on('error', function(err) {
            console.error(err);
        });
    },

    makePeerCall(peerid) {
        console.log("call with  " + peerid);
        this.peerNetworks[peerid].onCall();
    },

    //Peer instance created
    onOpen() {
        console.error("peer SuccessFully Done #############################" + window.peer.id);
        this.myPeerId = window.peer.id;
        this.eventEmitter.emit(Constants.VIDEOCALL_EVENTS.ON_OPEN, window.peer.id);
    },

    onReceiveStream(stream, element_id) {
        console.log("elemtn id set with mycamera " + element_id + " stream " + stream);
        var video = document.getElementById(element_id);
        video.srcObject = stream;
    },

    onConnectionRequest(conn) {
        // console.log("onConnectionRequest called with peer " + conn.peer);
        //  this.peerNetworks[conn.peer].startNetwork(this.userName);
    },

    onUserResponse(accepted, peerid) {
        console.log("user Response called with " + accepted + "  perid " + peerid);

        if (accepted) {
            if (window.localStream === null || peerid === undefined || peerid === "")
                return;
            this.peerNetworks[peerid].startNetwork(this.userName);
            return;
        }
        if (this.peerNetworks.hasOwnProperty(peerid))
            this.peerNetworks[peerid].closeConnection(false);
    },

    connectToPeer(peerid) {
        if (window.localStream === null || peerid === "")
            return;
        if (this.peerNetworks.hasOwnProperty(peerid)) {
            console.log("Already have a connection for peerid or peer Id is empty " + peerid);
            return;
        }
        let cameraId = this.getFreeCameraId();
        console.log("connectToPeer called with " + peerid + " camera Id " + cameraId);
        if (cameraId == -1) {
            console.log("network not created for peerid " + peerid);
            return;
        }
        // if (this.videopanel.children[cameraId].classList.contains("hidden"))
        //     this.videopanel.children[cameraId].classList.remove("hidden");
        if (this.videopanel.children[cameraId - 1].classList.contains("hidden"))
            this.videopanel.children[cameraId - 1].classList.remove("hidden");
        this.peerNetworks[peerid] = new PeerNetwork(cameraId, peerid);
    },


    getFreeCameraId() {
        for (let i = 0; i < this.busyCameras.length; i++) {
            if (!this.busyCameras[i]) {
                this.busyCameras[i] = true;
                return i + 1;
            }
        }
        console.log("No Free Camera Available");
        return -1;
    },


    //Ids of all Peers
    connectToPeers(peers) {
        console.log("Connect with multiple peers ", peers + " this.peerNetworks " + Object.keys(this.peerNetworks).length);
        for (let i = 0; i < peers.length; i++) {
            if (!this.peerNetworks.hasOwnProperty(peers[i]) && peers[i] !== "") {
                let cameraId = this.getFreeCameraId();
                console.log("network created for " + peers[i] + " CameraId is " + cameraId);
                if (cameraId == -1) {
                    console.log("network not created for peerid " + peers[i]);
                    return;
                }
                if (this.videopanel.children[cameraId - 1].classList.contains("hidden"))
                    this.videopanel.children[cameraId - 1].classList.remove("hidden");
                this.peerNetworks[peers[i]] = new PeerNetwork(cameraId, peers[i]);
                this.unMuteAllConnections();
            }
        }
    },

    disconnectPeer(peerid) {
        console.log("disconnect peer with peerid " + peerid + " called");
        if (!this.peerNetworks.hasOwnProperty(peerid)) {
            console.log("no connection regarding peerid " + peerid + " to disconnect");
        }
        this.peerNetworks[peerid].closeConnection(false);
        this.busyCameras[this.peerNetworks[peerid].cameraId - 1] = false;
        delete this.peerNetworks[peerid];
    },

    disconnectAllPeers() {
        if (window.localStream === null || window.localStream === undefined)
            return;
        console.log("disconnect all peers connection called");

        let allPeers = Object.keys(this.peerNetworks);
        for (let i = 0; i < allPeers.length; i++) {
            this.peerNetworks[allPeers[i]].closeConnection(true);
            this.busyCameras[this.peerNetworks[allPeers[i]].cameraId - 1] = false;
        }
        this.peerNetworks = {};
        if (this.videopanel.classList.contains("videopanelactive")) {
            console.log("disabled video panel");
            this.gameVideoToggle();
        }

        // let videoChilds = this.videopanel.children;
        // for (let p = 1; p < videoChilds.length; p++) {
        //     if (!videoChilds[p].classList.contains("hidden"))
        //         videoChilds[p].classList.add("hidden");
        // }
    },

    leavePeer(videoContainer) {
        let cameraId = videoContainer.id[4];
        console.log("camera id for leave " + cameraId);
        this.superToggle(videoContainer, "hidden", "");
        let allPeers = Object.keys(this.peerNetworks);
        let peerid = "";
        for (let i = 0; i < allPeers.length; i++) {
            console.log("In Leave Peer " + this.peerNetworks[allPeers[i]].cameraId);
            console.log("equal is " + (this.peerNetworks[allPeers[i]].cameraId == cameraId));
            if (this.peerNetworks[allPeers[i]].cameraId == cameraId) {
                peerid = allPeers[i];
                console.log("peerid aaaaaaaaaaaaaaaaa " + peerid);
                break;
            }
        }

        console.log("peerdiiiiiiiiiiiii " + peerid);
        if (peerid === "") {
            console.log("No One Leave because no able to find cameraid " + cameraId);
            return;
        }
        this.disconnectPeer(peerid);
    },
    ////////////HTML APIS////////////////

    destroyAllNetworks() {
        this.disconnectAllPeers();
        //document.getElementById("togglegame").classList.add("hidden");
        for (let i = 0; i < this.maxClients; i++) {
            this.disconnectPeerView((i + 1));
        }
    },

    muteByCameraId(cameraId) {
        let mediaPlayer = document.getElementById("peer" + cameraId + "-camera");
        mediaPlayer.muted = true;
    },

    unMuteAllConnections() {
        let allNetworks = Object.keys(this.peerNetworks);
        console.log("unMuteAllConnections called " + allNetworks.length);
        for (let i = 0; i < allNetworks.length; i++) {
            console.error("netwokr is " + allNetworks[i]);

            let id = "peer" + this.peerNetworks[allNetworks[i]].cameraId + "-camera";
            let mediaPlayer = document.getElementById(id);
            mediaPlayer.muted = false;
            let microBtn = mediaPlayer.parentNode.children[1];
            let videoBtn = mediaPlayer.parentNode.children[2];
            if (microBtn.classList.contains("microphoneoff")) {
                this.superToggle(microBtn, "microphoneon", "microphoneoff");
            }
            if (videoBtn.classList.contains("videooff")) {
                this.superToggle(videoBtn, "videoon", "videooff");
            }

            if (microBtn.classList.contains("hidden")) {
                microBtn.classList.toggle("hidden");
            }

            if (videoBtn.classList.contains("hidden")) {
                videoBtn.classList.toggle("hidden");
            }
        }


    },

    disconnectPeerView(cameraId) {
        if (window.localStream === null || window.localStream === undefined)
            return;
        console.log("camera Id in on Disconnecr " + cameraId);
        let mediaPlayer = document.getElementById("peer" + cameraId + "-camera");

    },

    disconnectWithPeerId(peerid) {
        if (this.peerNetworks.hasOwnProperty(peerid)) {
            this.disconnectPeerView(this.peerNetworks[peerid].cameraId);
            return;
        }
    },

    videoChatStatus(videoType, cameraID) {
        if (cameraID === "my") {
            this.eventEmitter.emit(Constants.VIDEOCALL_EVENTS.INTERNAL_VIDEO_STATUS, videoType);
        }
    },

    checkNetworkConnected(peerId) {
        if (window.peer.id === "" || Object.keys(window.VideoChatManager.peerNetworks).length === 0)
            return false;

        return this.peerNetworks[peerId].isConnected();
    },

    gameVideoToggle() {
        try {
            this.superToggle(this.videopanel, "videopanelactive", "videopaneldeactive");
            this.superToggle(this.gameCanvas, "gamepanelactive", "gamepaneldeactive");
        } catch (e) {
            console.log("Error in my game Video togglew ", e);
        }
    },

    isVideoPanelActive() {
        return this.videopanel.classList[this.videopanel.classList.length - 1] === "videopanelactive";
    },

    changeCloseBtnColor(isWhite = true) {
        this.closeBtn.className = isWhite ? "closeBtnBlack" : "closeBtnWhite";
    },

    enableVideoStream(value) {
        let allNetworks = Object.keys(this.peerNetworks);
        this.peerNetworks[allNetworks[0]].enableVideoStream();
    },

    enableAudioStream(cameraId) {
        let allNetworks = Object.keys(this.peerNetworks);
        this.peerNetworks[allNetworks[0]].enableAudioStream();
    },

    onSteamStatusChanged(status) {
        this.changeCloseBtnColor(status);
    }





});