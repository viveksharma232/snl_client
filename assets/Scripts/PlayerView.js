import Dice from './Dice';
const Constants = require("./VideoChat/PeerConstants")
cc.Class({
    extends: cc.Component,

    properties: {

        nameLbl: {
            type: cc.Label,
            default: null
        },
        scoreLbl: {
            type: cc.Label,
            default: null
        },
        avatar: {
            type: cc.Sprite,
            default: null
        },
        activeTurn: {
            type: cc.Node,
            default: null
        },
        waitUi: {
            type: cc.Node,
            default: null
        },
        turnSprite: {
            type: cc.Sprite,
            default: null
        },
        timerLbl: {
            type: cc.Label,
            default: null
        },
        disconnected: {
            type: cc.Node,
            default: null
        },
        playerIcon: {
            type: cc.Node,
            default: null
        },
        dice: {
            default: null,
            type: Dice
        },
        micSprite: {
            default: [],
            type: cc.SpriteFrame
        },
        mic: {
            default: null,
            type: cc.Sprite
        },
        videoSprite: {
            default: [],
            type: cc.SpriteFrame
        },
        video: {
            default: null,
            type: cc.Sprite
        },
        acceptBtn: {
            default: null,
            type: cc.Sprite
        },
        rejectBtn: {
            default: null,
            type: cc.Sprite
        },
        videoControls: {
            default: null,
            type: cc.Node
        },
        callBtn: {
            default: null,
            type: cc.Sprite
        }
    },


    start() {
        // this.timerOn = false;
        this.timerValue = 10;
        // this.timerLbl.string = this.timerValue;
        this.isClosed = true;
    },

    onEnable() {
        this.disconnected.active = false;
        this.waitUi.active = false;
        this.rejectBtn.node.active = false;
        this.acceptBtn.node.active = false;
        this.callBtn.node.active = false;
        this.videoControls.active = false;
        this.isUser = false;
        this.changeSpriteState({
            "audio": false,
            "video": false
        });
    },

    setPlayer(playerInfo, isUser) {
        this.isUser = isUser;
        this.nameLbl.string = playerInfo.name;
        this.setCallView();
        this.avatar.spriteFrame = Avatars.avatarSprites[playerInfo.avatarId];
        this.turnSprite.fillRange = 0;
        let peerIds = [];
        if (window.opponentPeerId !== undefined && window.opponentPeerId !== "" && isUser)
            peerIds.push(window.opponentPeerId);
        window.VideoChatManager.connectToPeers(peerIds);
        this.deactivateTurn();
    },

    activateTurn(elapsedTime = 0) {
        //this.activeTurn.active = true;
        this.turnSprite.node.active = true;
        this.turnSprite.fillRange = 1;
        this.waitUi.active = false;
        //if (this.room.state.currentTurn === this.myId)
        this.startTurnTimer(elapsedTime);
    },

    deactivateTurn() {
        //this.activeTurn.active = false;
        this.turnSprite.node.active = true;
        // this.dice.node.active = false;
        this.waitUi.active = true;
        this.timerOn = false;
        // this.stopTimer();
    },

    disablePlayer() {
        this.activeTurn.active = false;
        this.timerOn = false;
    },

    startTurnTimer(elapsedTime = 0) {
        this.timerValue = 10 - elapsedTime;
        // this.timerLbl.string = this.timerValue;
        this.timerOn = true;
    },

    onDisconnected() {
        this.disconnected.active = true;
    },

    onReconnected() {
        this.disconnected.active = false;
    },

    update(dt) {
        if (this.timerOn) {
            this.timerValue -= dt;
            this.turnSprite.fillRange -= 0.1 * dt;
            if (this.timerValue < 0) {
                this.stopTimer();
                SoundManager.stopSound();
            }
            // this.timerLbl.string = Math.ceil(this.timerValue);
        }
    },

    changeSpriteState(dataType) {

        if (dataType.audio) {
            this.mic.spriteFrame = this.micSprite[0];
        } else {
            this.mic.spriteFrame = this.micSprite[1];
        }
        if (dataType.video) {
            this.video.spriteFrame = this.videoSprite[0];
        } else {
            this.video.spriteFrame = this.videoSprite[1];
        }
        if (videoEnabled)
            this.videoControls.active = (dataType.audio || dataType.video);

    },

    onProfileClick() {
        if (window.VideoChatManager.isVideoPanelActive() || !window.VideoChatManager.checkNetworkConnected(window.opponentPeerId))
            return;
        window.VideoChatManager.gameVideoToggle();
    },

    stopTimer() {
        this.turnSprite.fillRange = 0;
        this.timerValue = 0;
        this.timerOn = false;
    },

    rollDice() {
        this.stopTimer();
        this.dice.onRoll();
    },

    onDiceOutCome(outCome) {
        // this.deactivateTurn();
        this.dice.onStop(outCome);
    },

    showAcceptBtn(show) {
        if (!videoEnabled)
            return;
        this.acceptBtn.node.active = show;
        this.rejectBtn.node.active = show;
        this.callBtn.node.active = show;
    },

    setCallView() {
        if (this.isUser || !videoEnabled)
            return;
        this.acceptBtn.node.active = false;
        this.rejectBtn.node.active = false;
        this.callBtn.node.active = true;
    },

    setCallReceiveView() {
        if (window.VideoChatManager.checkNetworkConnected(window.opponentPeerId) || !videoEnabled)
            return;
        this.acceptBtn.node.active = true;
        this.rejectBtn.node.active = true;
        this.callBtn.node.active = false;
    },

    onAcceptClick() {
        this.requestLocalVideo({
            success: (function (stream) {
                stream.getTracks().forEach(function (track) {
                    console.error("TRacked     ", track.getSettings());
                })
                window.localStream = stream;
                window.VideoChatManager.eventEmitter.emit(Constants.VIDEOCALL_EVENTS.ON_MAKE_CALL);
                window.VideoChatManager.onUserResponse(true, window.opponentPeerId);
                this.showAcceptBtn(false);
            }).bind(this),
            error: (function (err) {
                window.localStream = null;
                window.VideoChatManager.onUserResponse(false, window.opponentPeerId);
                PopUpManager.show(PopUpType.Permission, {});
                this.setCallView();
            }).bind(this)
        });
        this.showAcceptBtn(false);
    },


    onRejectClick() { //If Rejected then user can make call again
        this.showAcceptBtn(false);
        window.VideoChatManager.onUserResponse(false, window.opponentPeerId);
    },

    requestLocalVideo(callbacks) {
        console.log("requestLocalVideo called ");
        navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
        var constraints = {
            audio: true,
            video: {
                width: 320,
                height: 240,
                aspectRatio: 1.3333,
                facingMode: "user",
                frameRate: 10
            }
        };
        navigator.mediaDevices.getUserMedia(constraints)
            .then(stream => {
                console.log("requestLocalVideo called getuser media then ");
                callbacks.success(stream);
            })
            .catch(err => {
                console.log("error in LocalVideo " + err);
                callbacks.error(err);
            })

    }


})