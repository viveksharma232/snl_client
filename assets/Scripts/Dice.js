cc.Class({
    extends: cc.Component,

    properties: {
        diceAnim: {
            // ATTRIBUTES:
            default: null,
            type: cc.Node,
        },
        diceSprites: {
            default: [],
            type: cc.SpriteFrame
        },
        diceBaseSprite: {
            default: null,
            type: cc.Sprite
        },
        diceShow: {
            default: [],
            type: cc.Sprite
        }
    },

    onRoll() {
        this.diceBaseSprite.enabled = true;
        this.diceAnim.getComponent(cc.Animation).play();
    },

    onStop(outCome) {
        console.log('On OutCome ' + outCome);
        this.diceAnim.getComponent(cc.Animation).stop();
        this.diceAnim.getComponent(cc.Sprite).spriteFrame = this.diceSprites[outCome - 1];
        setTimeout(() => {
            this.diceBaseSprite.enabled = false;
            //this.diceShow[0].spriteFrame = this.diceShow[1].spriteFrame = this.diceSprites[outCome-1];
        }, 1000);
    }

});